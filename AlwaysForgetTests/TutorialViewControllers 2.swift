//
//  TutorialViewControllers.swift
//  AlwaysForget
//
//  Created by User on 3/08/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
private let MaskViewTagForItem:Int = 16
private let MaskViewTagForScene:Int = 15
private let MaskViewTagForDraging:Int = 14
private let MaskViewTagForSaving = 13
enum TutorialState:Int{
    case ItemAdding
    case SceneAdding
    case Draging
    case Saving
}
private let TutorialFinishedAlertViewTitleStr:String = NSLocalizedString("Congradulations",comment: "Tutorial Finished Alert View Title Str")
private let TutorialFinishedAlertViewMessageStr:String = NSLocalizedString("Zhao Saved",comment: "Tutoril Finished Alert View Message Str")
private let TutorialFinishedAlertViewCancelButtonTitleStr:String = NSLocalizedString("OK",comment: "Tutorial Finished Alert View Cancel Button Title Str")
private let SaveStr:String = NSLocalizedString("Save",comment: "Save button title in Tutorial")
private let CancelStr:String = NSLocalizedString("Cancel",comment: "Cancel title in Tutorial")
private let ZhaoStr:String = NSLocalizedString("Zhao",comment: "Zhao title in Tutorial")

class TutorialHomeViewController:HomeViewController{
    //lazy var titleLabel
    lazy var maskView:UIImageView = {
        let view = UIImageView(frame: CGRect(x: self.addNewBtnBg!.center.x, y: self.addNewBtnBg!.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        return view
        }()
    override func btnAddNewTouched(){
        self.navigationController?.pushViewController(TutorialAddingNewViewController(), animated: true)
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.naviSettingBtn?.removeFromSuperview()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.userInteractionEnabled = false
        self.collectionViewCategories!.userInteractionEnabled = false
        self.categoryIndicator?.userInteractionEnabled = false
        self.view.backgroundColor = UIColor.clearColor()
        self.view.addSubview(self.maskView)
    }
}

class TutorialAddingNewViewController:AddingNewViewController{

    lazy var titleLabel:UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = RecordStr
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(18)
        return naviTitleLable
        }()
    var maskView:UIView?
    lazy var maskViewForItem:UIView = {
        let view = UIImageView(frame: CGRect(x: self.itemView.center.x, y: self.itemView.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        view.tag = MaskViewTagForItem
        return view
        }()
    lazy var maskViewForScene:UIView = {
        let view = UIImageView(frame: CGRect(x: self.sceneView.center.x, y: self.sceneView.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        //view.image = UIImage(named: "TutorialMask")
        view.tag = MaskViewTagForScene
        return view
        }()
    lazy var maskViewForDraging:UIImageView = {
        let view = UIImageView(frame: CGRect(x: self.itemView.center.x, y: self.itemView.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        view.tag = MaskViewTagForDraging
        return view
        }()
    lazy var maskViewForSaving:UIView = {
        let view = UIImageView(frame: CGRect(x: bgWidth - 60 , y: self.navigationController!.navigationBar.frame.origin.y + 10, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        view.tag = MaskViewTagForScene
        return view

        }()
    var tutorialState:TutorialState = TutorialState.ItemAdding {
        willSet{
            switch newValue{
            case .ItemAdding:
                self.maskView?.removeFromSuperview()
                self.maskView = self.maskViewForItem
                self.sceneCmrBtn.userInteractionEnabled = false
                self.view.addSubview(self.maskView!)
                SZLOG("maskViewForItem")
            case .SceneAdding:
                self.sceneCmrBtn.userInteractionEnabled = true
                self.maskView?.removeFromSuperview()
                self.maskView = self.maskViewForScene
                self.itemImg.userInteractionEnabled = false
                self.btns.first?.userInteractionEnabled = false
                self.btns.last?.userInteractionEnabled = false
                self.itemCmrBtn.userInteractionEnabled = false
                self.view.addSubview(maskView!)
                SZLOG("maskViewForScene")
            case .Draging:
                self.sceneView.userInteractionEnabled = false
                self.maskView?.removeFromSuperview()
                self.maskView = self.maskViewForDraging
                self.view.addSubview(maskView!)
                self.itemCmrBtn.userInteractionEnabled = true
                self.itemImg.userInteractionEnabled = true
                SZLOG("maskViewForDraging")
            case .Saving:
                self.maskView?.removeFromSuperview()
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                self.maskView = self.maskViewForSaving
                self.itemCmrBtn.userInteractionEnabled = false
                self.sceneCmrBtn.userInteractionEnabled = false
                self.btns.first?.userInteractionEnabled = false
                self.btns.last?.userInteractionEnabled = false
                self.view.addSubview(self.maskView!)
                SZLOG("maskViewForSaving")
            default: SZLOG("unknown error")
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.maskView = self.maskViewForItem
        self.sceneCmrBtn.userInteractionEnabled = false
        self.view.addSubview(self.maskView!)
        self.view.addSubview(self.titleLabel)

    }
    
    override func btnSaveTouched() {
        super.btnSaveTouched()
        let alertView = UIAlertView(title: TutorialFinishedAlertViewTitleStr, message: TutorialFinishedAlertViewMessageStr, delegate: nil, cancelButtonTitle: TutorialFinishedAlertViewCancelButtonTitleStr)
        alertView.show()
    }
    
    //MARK:Event and State transfer
    override func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        super.imagePickerController(picker, didFinishPickingMediaWithInfo: info)
        //self.tutorialState = TutorialState.Draging
        switch self.typeOfAcquiredPhoto{
        case PhotoType.Scene:
            self.tutorialState = TutorialState.Draging
        case PhotoType.Item:
            self.tutorialState = TutorialState.SceneAdding
        default:
            SZLOG("unknown error")
        }
    }
    override func existedScenesPickerController(existedScenesPicker picker:ExistedScenesPickerController,didFinishPickingScene scene:SceneMO){
        super.existedScenesPickerController(existedScenesPicker: picker, didFinishPickingScene: scene)
        switch self.typeOfAcquiredPhoto{
        case PhotoType.Scene:
            self.tutorialState = TutorialState.Draging
        case PhotoType.Item:
            self.tutorialState = TutorialState.SceneAdding
        default:
            SZLOG("unknown error")
        }
    }
    override func saveCategoryAndDetail(popupWin: PopupWinType) {
        self.tutorialState = TutorialState.Saving
    }
    
}
