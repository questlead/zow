////
////  ExistedScenePicker.swift
////  AlwaysForget
////
////  Created by User on 15/08/2015.
////  Copyright (c) 2015 Silversr. All rights reserved.
////
//
//import UIKit
//
//extension UIImage{
//    func croppedImage(imageSizeWidth width:CGFloat = 500,imageSizeHeight height:CGFloat = 500) -> UIImage{
//        let size = CGSizeMake(width, height)
//        let scale:CGFloat = max(size.width/self.size.width, size.height/self.size.height)
//        let scaledWidth:CGFloat = self.size.width * scale;
//        let scaledHeight:CGFloat = self.size.height * scale;
//        let imageRect:CGRect = CGRectMake((size.width - scaledWidth)/CGFloat(2.0),(size.height - scaledHeight)/CGFloat(2.0),scaledWidth, scaledHeight);
//        UIGraphicsBeginImageContextWithOptions(size, false, 0)
//        self.drawInRect(imageRect)
//        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return newImage;
//    }
//    func thumbnailImage() -> UIImage{
//        return self.croppedImage(imageSizeWidth: 320, imageSizeHeight: 320)
//    }
//}
//@objc protocol Deletable{
//    var selecting:Bool {get set}
//    var toBeDeletedCellIndexPaths:[NSIndexPath] {get set}
//    optional var toBeDeletedScenes:[SceneMO] {get set}
//    optional var toBeDeletedItems:[ItemMO] {get set}
//    optional var toBeDeletedCategories:[CategoryMO] {get set}
//    func deleteCells()
//}
////MARK: Basic function: show collection view
//class CollectionDataManager: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
//    //MARK:Properties
//    lazy internal var collectionViewDataArray:[AnyObject]? = {
//        return nil
//        }()
//    lazy internal var collectionViewLayoutItemSize:CGSize? = CGSizeMake(UIScreen.mainScreen().bounds.width / 2 - 1, UIScreen.mainScreen().bounds.width / 2 - 1)
//    lazy internal var collectionViewLayourMinimumInteritemSpacing:CGFloat? = CGFloat(2)
//    lazy internal var collectionViewLayourMinimumLineSpacing:CGFloat? = CGFloat(2)
//    lazy private var collectionViewLayout:UICollectionViewFlowLayout = {
//        let layout = UICollectionViewFlowLayout()
//        layout.itemSize = self.collectionViewLayoutItemSize!
//        layout.minimumInteritemSpacing = self.collectionViewLayourMinimumInteritemSpacing!
//        layout.minimumLineSpacing = self.collectionViewLayourMinimumLineSpacing!
//        layout.scrollDirection = UICollectionViewScrollDirection.Vertical
//        return layout
//    }()
//    lazy internal var collectionViewFrame:CGRect = {
//        return CGRect(x: 0, y: 64, width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height - 64)
//        }()
//    lazy private var collectionView:UICollectionView = {
//        let collectionView = UICollectionView(frame: self.collectionViewFrame, collectionViewLayout: self.collectionViewLayout)
//        collectionView.backgroundColor = UIColor.whiteColor()
//        collectionView.delegate = self
//        collectionView.dataSource = self
//        return collectionView
//    }()
//    //MARK: UICollectionView DataSource
//    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
//        return collectionViewDataArray?.count ?? 0
//    }
//    
//    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
//        return UICollectionViewCell()
//    }
//    //MARK: UIViewController Delegate
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.view.addSubview(self.collectionView)
//        
//    }
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//}
