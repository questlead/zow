//
//  FeedbackRatingCell.swift
//  AlwaysForget
//
//  Created by User on 26/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

class FeedbackRatingCell: UICollectionViewCell {
    
    var rateImgView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        rateImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        rateImgView.image = UIImage(named: "Unclass")
        
        self.addSubview(rateImgView)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
