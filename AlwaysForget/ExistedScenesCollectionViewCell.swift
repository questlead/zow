//
//  ExistedScenesCollectionViewCell.swift
//  AlwaysForget
//
//  Created by User on 19/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
let alertViewForDeletingSceneTitle:String = NSLocalizedString("Warning!",comment: "Alert View For Deleting Scene: Title")
private let alertViewForDeletingSceneMessage:String = NSLocalizedString("Deleting this scene will result in all item related to this scene deleted",comment: "Alert View For Deleting Scene: Message")

class ExistedScenesCollectionViewCell: UICollectionViewCell,UIAlertViewDelegate {

    //related scene
    var assoicatedSceneMO:SceneMO!
    //ges rec
    var longPressGestureRecognizer: UILongPressGestureRecognizer?
    //alertView for deleting scenes
    lazy var alertViewForDeletingScene:UIAlertView? = {
        return UIAlertView(title: alertViewForDeletingSceneTitle, message: alertViewForDeletingSceneMessage, delegate: self, cancelButtonTitle: cancelStr, otherButtonTitles: popupOkBtnStr)
        }()
    
    var imgView : UIImageView?
    var deleteBtn: UIButton?
    var isDeleteBtnHidden = true{
        willSet{
            if newValue == false{
                self.delegate?.enableTapGesture!()
            }else{
                self.delegate?.disenableTapGesture!()
            }
        }
    }
    lazy var selectBtn: UIButton = {
        var selectBtn = UIButton(frame: CGRect(x: bgWidth/2 - 40, y: bgWidth/2 - 40, width: 40, height: 40))
        selectBtn.addTarget(self, action: "selectBtnTouched", forControlEvents: UIControlEvents.TouchUpInside)
        selectBtn.setBackgroundImage(UIImage(named: "select"), forState: UIControlState.Normal)
        selectBtn.hidden = true
        selectBtn.userInteractionEnabled = false
        return selectBtn
        }()
    var isSelectBtnSelected = false{
        willSet{
            if newValue == false{
                selectBtn.setBackgroundImage(UIImage(named: "select"), forState: UIControlState.Normal)
            }else{
                selectBtn.setBackgroundImage(UIImage(named: "selectSelected"), forState: UIControlState.Normal)
            }
        }
    }
    weak var delegate: ExistedScenesPickerControllerDelegate?
    
    //MARK:Init and Deinit
    override init(frame: CGRect) {
        super.init(frame: frame)
        //scale to fit
        
        //long press related
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPressGesture:")
        longPressGestureRecognizer!.numberOfTouchesRequired = 1
        longPressGestureRecognizer!.allowableMovement = 50
        longPressGestureRecognizer!.minimumPressDuration = 0.5
        
        imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        //imgView?.contentMode = UIViewContentMode.Center
        deleteBtn = UIButton(frame: CGRect(x: frame.width - 40, y: 0, width: 40, height: 40))
        //deleteBtn?.backgroundColor = UIColor.redColor()
        //deleteBtn?.layer.cornerRadius = deleteBtn!.frame.height/2
        deleteBtn?.setBackgroundImage(UIImage(named: "delete"), forState: UIControlState.Normal)
        deleteBtn?.addTarget(self, action: "deleteBtnTouched", forControlEvents: UIControlEvents.TouchUpInside)
        deleteBtn?.hidden = true
        
        isDeleteBtnHidden = deleteBtn!.hidden

        //self.addGestureRecognizer(longPressGestureRecognizer!)
        self.addSubview(imgView!)
        self.addSubview(deleteBtn!)
        self.addSubview(selectBtn)
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    deinit {
        
    }
    //MARK:Long Press Recognizer
    func handleLongPressGesture(recognizer:UILongPressGestureRecognizer){
        if (recognizer.state == UIGestureRecognizerState.Began){
            SZLOG("long press")
            deleteBtn!.hidden = false
            //var notification = NSNotification(name: "CellLongPress", object: self.assoicatedSceneMO.localIdentifier)

            longPressGestureRecognizer?.enabled = false
            isDeleteBtnHidden = deleteBtn!.hidden
        }
    }
    //MARK: delete scenes and related alert view
    func deleteBtnTouched(){
        self.alertViewForDeletingScene?.show()
    }
    
    func selectBtnTouched(){
        self.isSelectBtnSelected = !self.isSelectBtnSelected
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        switch buttonIndex{
        case 0:
            SZLOG("cancel button click")
            deleteBtn!.hidden = true
            isDeleteBtnHidden = deleteBtn!.hidden
        case 1:
            SZLOG("confirm button click")
            deleteBtn!.hidden = true
            isDeleteBtnHidden = deleteBtn!.hidden
            SceneMO.deleteScene(scene: self.assoicatedSceneMO)
            let notificationForDeletingAScene = NSNotification(name: "DeleteScene", object: self.assoicatedSceneMO)
            notificationCenter.postNotification(notificationForDeletingAScene)
        default:
            SZLOG("unkown error")
        }
    }

}
