//
//  FeedbackPopupViewController.swift
//  AlwaysForget
//
//  Created by User on 26/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

private let TitleText = NSLocalizedString("Feedback",comment: "Feedback Popup Window Title")
private let ratingLabelStr = NSLocalizedString("Please stars.",comment: "Rating tip label string")
private let commentLabelStr = NSLocalizedString("Your advice matters.",comment: "Comment tip label string")
private let naviSendBtnStr = NSLocalizedString("Deliver", comment: "Navigation button title")
private let FeedbackAlertStr = NSLocalizedString("Feedback has been saved and sent.",comment: "feed back successful.")
private let FeedbackSuccessfulStr = NSLocalizedString("Successful!",comment: "feed back successful title")
private let FeedbackOKStr = NSLocalizedString("OK!",comment: "feed back successful OK button")


class FeedbackPopupViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate,UITextViewDelegate {

    var naviSend : UIBarButtonItem?
    var flowLayOut = UICollectionViewFlowLayout()
    var collectionViewRating: UICollectionView?
    var commentView  : UITextView?
    var ratingLabel  : UILabel?
    var commentLabel : UILabel?
    var rate = Int(5)
    var comment = ""
    
    lazy var naviBarView:UIView = {
        let naviBarView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: 64))
        naviBarView.backgroundColor = UIColor.myBlueColor()
        return naviBarView
        }()
    lazy var naviTitleLable : UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = TitleText
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(20)
        return naviTitleLable
        }()
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        commentView?.resignFirstResponder()
        if commentView?.isFirstResponder() == true{
            SZLOG("is first responder ")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.myWhiteColor()
        self.view.addSubview(naviBarView)

        ratingLabel = UILabel(frame: CGRect(x: 0, y: 75, width: bgWidth, height: 30))
        ratingLabel?.text = ratingLabelStr
        ratingLabel?.textAlignment = NSTextAlignment.Center
        ratingLabel?.textColor = UIColor.myBrownColor()
        
        flowLayOut.itemSize = CGSizeMake(50, 50)
        flowLayOut.minimumInteritemSpacing = 0
        flowLayOut.minimumLineSpacing = 0
        collectionViewRating = UICollectionView(frame: CGRect(x: (bgWidth - 50*5)/2, y: 75 + 30, width: 50*5, height: 50), collectionViewLayout: flowLayOut)
        collectionViewRating?.backgroundColor = UIColor.myWhiteColor()
        collectionViewRating?.dataSource = self
        collectionViewRating?.delegate   = self
        
        commentLabel = UILabel(frame: CGRect(x: 0, y: 75 + 30 + 50 + 15, width: bgWidth, height: 30))
        commentLabel?.text = commentLabelStr
        commentLabel?.textAlignment = NSTextAlignment.Center
        commentLabel?.textColor = UIColor.myBrownColor()
        
        commentView = UITextView(frame: CGRect(x: 10, y: 75 + 30 + 50 + 15 + 30 + 5, width: bgWidth - 20, height: 100))
        commentView?.delegate = self
        commentView?.layer.borderColor = UIColor.lightGrayColor().CGColor
        commentView?.layer.borderWidth = 1
        commentView?.text = comment
        
        self.view.addSubview(naviTitleLable)
        self.view.addSubview(ratingLabel!)
        self.view.addSubview(collectionViewRating!)
        self.view.addSubview(commentView!)
        self.view.addSubview(commentLabel!)
        
        naviSend = UIBarButtonItem(title: naviSendBtnStr, style: UIBarButtonItemStyle.Plain, target: self, action: "btnSendTouched")
        self.navigationItem.rightBarButtonItem = naviSend
    }
    
    func btnSendTouched(){
        SZLOG("Rate: \(rate) + Comment: \(comment)")
        //键盘dismiss
//        if commentView!.isFirstResponder(){
//            commentView?.resignFirstResponder()
//        }
        //commentView?.endEditing(true)
        //commentView?.becomeFirstResponder()
        //commentView?.resignFirstResponder()
        //发送成功到英文没翻译
        Feedback.sendToServer(Feedback(rating: rate, comments: comment))
        var alertView = UIAlertView(title: FeedbackSuccessfulStr, message: FeedbackAlertStr, delegate: self, cancelButtonTitle: FeedbackOKStr)
        alertView.show()
    }
    
    //MARK: CollectionView Func
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerClass(FeedbackRatingCell.self, forCellWithReuseIdentifier: "feedbackCell")
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("feedbackCell", forIndexPath: indexPath) as! FeedbackRatingCell
        if indexPath.item < rate {
            cell.rateImgView.image = UIImage(named: "rateSelected")
        }else{
            cell.rateImgView.image = UIImage(named: "rate")
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.commentView?.resignFirstResponder()
        SZLOG("feedback rating selected")
        rate = indexPath.item + 1
        collectionView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //AlertView delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex == 0{
//            var transition = CATransition()
//            transition.duration = 0.2
//            transition.type = kCATransitionPush
//            transition.subtype = kCATransitionFromLeft
//            self.navigationController?.view.layer.addAnimation(transition, forKey: nil)
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    //textview delegate
//    func textViewDidChange(textView: UITextView){
//        SZLOG("text view did changed to \(textView.text)")
//        self.comment = textView.text
//    }
    func textViewDidEndEditing(textView: UITextView) {
        SZLOG("textview did end.")
        self.comment = textView.text
    }
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.commentView?.resignFirstResponder()
    }

}
