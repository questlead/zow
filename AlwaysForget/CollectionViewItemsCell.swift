//
//  CollectionViewItemsCell.swift
//  AlwaysForget
//
//  Created by User on 12/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

class CollectionViewItemsCell: UICollectionViewCell {
    
    var itemImage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        itemImage = UIImageView(frame: CGRect(x: 0, y: 0, width: bgWidth/4 - 5, height: bgWidth/4))
        itemImage.image = UIImage()
        
        self.addSubview(itemImage)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
