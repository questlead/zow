//
//  Feedback.swift
//  AlwaysForget
//
//  Created by User on 23/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import Foundation
import Parse

class Feedback:NSObject {
    init(rating:Int!, comments:String){
        super.init()
        if rating <= 5 && rating >= 1{
            self.rating = rating
            self.comments = comments
        }else{
            SZLOG("Error: rating number is out of range")
        }

    }
    var rating:Int = 5
    var comments:String = "Excellent!"
    class func sendToServer(feedback:Feedback){
        let fdbk = PFObject(className: "Feedback")
        fdbk["rating"] = feedback.rating
        fdbk["comments"] = feedback.comments
        fdbk.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            SZLOG("Feedback has been saved and sent.")
        }
    }
}