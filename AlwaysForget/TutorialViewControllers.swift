//
//  TutorialViewControllers.swift
//  AlwaysForget
//
//  Created by User on 3/08/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
private let MaskViewTagForItem:Int = 16
private let MaskViewTagForScene:Int = 15
private let MaskViewTagForDraging:Int = 14
private let MaskViewTagForSaving = 13
enum TutorialState:Int{
    case ItemAdding
    case SceneAdding
    case Draging
    case Saving
}
private let TutorialFinishedAlertViewTitleStr:String = NSLocalizedString("Congradulations",comment: "Tutorial Finished Alert View Title Str")
private let TutorialFinishedAlertViewMessageStr:String = NSLocalizedString("Zow Saved",comment: "Tutoril Finished Alert View Message Str")
private let TutorialFinishedAlertViewCancelButtonTitleStr:String = NSLocalizedString("OK",comment: "Tutorial Finished Alert View Cancel Button Title Str")
private let TutorialCommentAddNewRecordStr:String = NSLocalizedString("Click \"Zow\" button to create new Zow.",comment: "TutorialCommentAddNewRecordStr")
private let TutorialCommentAddItemStr:String = NSLocalizedString("Click \"Item\" camera to add item picture.",comment: "TutorialCommentAddItemStr")
private let TutorialCommentAddSceneStr:String = NSLocalizedString("Click \"Scene\" camera to add Scene picture.",comment: "TutorialCommentAddSceneStr")
private let TutorialCommenDragStr:String = NSLocalizedString("Long press and drag item to scene for locating.",comment: "TutorialCommenDragStr")
private let TutorialCommenSaveStr:String = NSLocalizedString("Save a Zow.",comment: "TutorialCommenSaveStr")

private let SaveStr:String = NSLocalizedString("Save",comment: "Save button title in Tutorial")
private let CancelStr:String = NSLocalizedString("Cancel",comment: "Cancel title in Tutorial")
private let ZhaoStr:String = NSLocalizedString("Zhao",comment: "Zhao title in Tutorial")

private func animationOfView(maskView:UIView){
    UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.Autoreverse | UIViewAnimationOptions.Repeat, animations: { () -> Void in
        maskView.transform = CGAffineTransformMakeScale(0.95, 0.95)
        maskView.frame.origin.x -= 10
        maskView.frame.origin.y -= 10
    }, completion: nil)
    var shadowWidthAnimation = CABasicAnimation(keyPath: "shadowOffset.width")
    shadowWidthAnimation.duration = 0.5
    shadowWidthAnimation.fromValue = -10
    shadowWidthAnimation.toValue = -3
    shadowWidthAnimation.autoreverses = true
    shadowWidthAnimation.repeatCount = MAXFLOAT
    maskView.layer.addAnimation(shadowWidthAnimation, forKey: "shadowOffset.width")
    var shadowHeightAnimation = CABasicAnimation(keyPath: "shadowOffset.height")
    shadowHeightAnimation.duration = 0.5
    shadowHeightAnimation.fromValue = -10
    shadowHeightAnimation.toValue = -3
    shadowHeightAnimation.autoreverses = true
    shadowHeightAnimation.repeatCount = MAXFLOAT
    maskView.layer.addAnimation(shadowHeightAnimation, forKey: "shadowOffset.height")
}


class TutorialHomeViewController:HomeViewController{
    //lazy var titleLabel
    //var timer = NSTimer()
    lazy var maskView:UIImageView = {
        let view = UIImageView(frame: CGRect(x: self.addNewBtnBg!.center.x, y: self.addNewBtnBg!.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        view.layer.shadowOffset = CGSizeMake(-10, -10)
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 0.2
        return view
        }()
    lazy var label:UILabel = {
        let label = UILabel(frame: CGRect(x: 70, y: self.addNewBtnBg!.center.y + 130, width: bgWidth - 140, height: 70))
        label.backgroundColor = UIColor.myOrangeColor()
        label.layer.cornerRadius = 8
        label.layer.masksToBounds = true
        label.text = TutorialCommentAddNewRecordStr
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.font = UIFont.systemFontOfSize(18)
        return label
        }()
    lazy var arrow: UIView = {
        let arrow = UIView(frame: CGRect(x: self.label.frame.origin.x + self.label.frame.width/2 - 20, y: self.label.frame.origin.y - 10, width: 20, height: 20))
        arrow.backgroundColor = UIColor.myOrangeColor()
        arrow.transform = CGAffineTransformMakeRotation(3.14/4)
        return arrow
    }()
    override func btnAddNewTouched(){
        self.navigationController?.pushViewController(TutorialAddingNewViewController(), animated: true)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.naviSettingBtn?.removeFromSuperview()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        timer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(3), target: self, selector: "AddNewBtnAnimation:", userInfo: nil, repeats: true)
//        timer.fire()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.userInteractionEnabled = false
        self.collectionViewCategories!.userInteractionEnabled = false
        self.categoryIndicator?.userInteractionEnabled = false
        self.view.backgroundColor = UIColor.clearColor()
        self.view.addSubview(self.maskView)
        
        self.view.addSubview(arrow)
        self.view.addSubview(self.label)
        
        animationOfView(self.maskView)
    }

}

class TutorialAddingNewViewController:AddingNewViewController{
    var maskView:UIView?
    lazy var maskViewForItem:UIView = {
        let view = UIImageView(frame: CGRect(x: self.itemView.center.x, y: self.itemView.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        view.tag = MaskViewTagForItem
        view.layer.shadowOffset = CGSizeMake(-10, -10)
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 0.2
        return view
        }()
    lazy var maskViewForScene:UIView = {
        let view = UIImageView(frame: CGRect(x: self.sceneView.center.x, y: self.sceneView.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        //view.image = UIImage(named: "TutorialMask")
        view.tag = MaskViewTagForScene
        view.layer.shadowOffset = CGSizeMake(-10, -10)
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 0.2
        return view
        }()
    lazy var maskViewForDraging:UIImageView = {
        let view = UIImageView(frame: CGRect(x: self.itemView.center.x, y: self.itemView.center.y, width: 100, height: 100))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "finger")
        view.tag = MaskViewTagForDraging
        view.layer.shadowOffset = CGSizeMake(-10, -10)
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 0.2
        return view
        }()
    lazy var maskViewForSaving:UIView = {
        let view = UIImageView(frame: CGRect(x: bgWidth - 110 , y: self.navigationController!.navigationBar.frame.origin.y + 40, width: 80, height: 80))
        view.userInteractionEnabled = false
        view.image = UIImage(named: "fingerLeft")
        view.tag = MaskViewTagForScene
        view.layer.shadowOffset = CGSizeMake(-10, -10)
        view.layer.shadowColor = UIColor.lightGrayColor().CGColor
        view.layer.shadowOpacity = 0.8
        return view

        }()
    lazy var longpress: UIView = {
        let longpress = UIView(frame: CGRect(x: self.itemView.center.x - 40, y: self.itemView.center.y - 55, width: 100, height: 100))
        longpress.backgroundColor = UIColor.myOrangeColor()
        longpress.layer.cornerRadius = longpress.frame.height/2
        longpress.alpha = 0
        return longpress
        }()
    lazy var tutorialLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: bgWidth - 140, height: 70))
        label.backgroundColor = UIColor.myOrangeColor()
        label.layer.cornerRadius = 8
        label.layer.masksToBounds = true
        label.text = ""
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.font = UIFont.systemFontOfSize(18)
        return label
        }()
    lazy var arrow: UIView = {
        let arrow = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        arrow.backgroundColor = UIColor.myOrangeColor()
        arrow.transform = CGAffineTransformMakeRotation(3.14/4)
        return arrow
        }()
    var timer:NSTimer!
    var tutorialState:TutorialState = TutorialState.ItemAdding {
        willSet{
            switch newValue{
            case .ItemAdding:
                self.maskView?.removeFromSuperview()
                self.maskView = self.maskViewForItem
                self.sceneCmrBtn.userInteractionEnabled = false
                self.view.addSubview(self.maskView!)
                animationOfView(self.maskViewForItem)
                tutorialLabel.text = TutorialCommentAddItemStr
                tutorialLabel.frame.origin.x = 30
                tutorialLabel.frame.origin.y = maskView!.frame.origin.y - 130
                arrow.center.x = tutorialLabel.frame.origin.x + tutorialLabel.frame.width/2 - 50
                arrow.center.y = tutorialLabel.frame.origin.y + tutorialLabel.frame.height
                SZLOG("maskViewForItem")
            case .SceneAdding:
                self.sceneCmrBtn.userInteractionEnabled = true
                self.maskView?.removeFromSuperview()
                self.maskView = self.maskViewForScene
                animationOfView(self.maskViewForScene)
                self.itemImg.userInteractionEnabled = false
                self.btns.first?.userInteractionEnabled = false
                self.btns.last?.userInteractionEnabled = false
                self.itemCmrBtn.userInteractionEnabled = false
                self.view.addSubview(maskView!)
                tutorialLabel.text = TutorialCommentAddSceneStr
                tutorialLabel.frame.origin.x = 70
                tutorialLabel.frame.origin.y = maskView!.frame.origin.y + 130
                arrow.center.x = tutorialLabel.frame.origin.x + tutorialLabel.frame.width/2 - 20
                arrow.center.y = tutorialLabel.frame.origin.y
                self.view.bringSubviewToFront(tutorialLabel)
                SZLOG("maskViewForScene")
            case .Draging:
                self.sceneView.userInteractionEnabled = false
                self.maskView?.removeFromSuperview()
                self.maskView = self.maskViewForDraging
                self.maskViewForDraging.layer.shadowOffset.width = -3
                self.maskViewForDraging.layer.shadowOffset.height = -3
                //self.view.addSubview(longpress)
                self.view.addSubview(maskView!)
                timer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(5), target: self, selector: "dragAnimations:", userInfo: nil, repeats: true)
                timer.fire()
                self.itemCmrBtn.userInteractionEnabled = true
                self.itemImg.userInteractionEnabled = true
                tutorialLabel.text = TutorialCommenDragStr
                tutorialLabel.frame.origin.x = 30
                tutorialLabel.frame.origin.y = 100
                arrow.center.x = tutorialLabel.frame.origin.x + tutorialLabel.frame.width/2 - 20
                arrow.center.y = tutorialLabel.frame.origin.y + tutorialLabel.frame.height
                SZLOG("maskViewForDraging")
            case .Saving:
                timer.invalidate()
                self.maskView?.removeFromSuperview()
                //longpress.removeFromSuperview()
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                self.maskView = self.maskViewForSaving
                animationOfView(self.maskViewForSaving)
                self.itemCmrBtn.userInteractionEnabled = false
                self.sceneCmrBtn.userInteractionEnabled = false
                self.btns.first?.userInteractionEnabled = false
                self.btns.last?.userInteractionEnabled = false
                self.view.addSubview(self.maskView!)
                tutorialLabel.text = TutorialCommenSaveStr
                tutorialLabel.frame.origin.x = 100
                tutorialLabel.frame.origin.y = 200
                arrow.center.x = tutorialLabel.frame.origin.x + tutorialLabel.frame.width/2 + 50
                arrow.center.y = tutorialLabel.frame.origin.y
                SZLOG("maskViewForSaving")
            default: SZLOG("unknown error")
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.maskView = self.maskViewForItem
        self.sceneCmrBtn.userInteractionEnabled = false
        self.view.addSubview(self.maskView!)
        self.view.addSubview(self.tutorialLabel)
        self.view.addSubview(arrow)
        
        //self.label?.hidden = true
        self.tutorialState = TutorialState.ItemAdding
    }
    func dragAnimations(timer:NSTimer){
        //indicatorAnimation()
        dragAnimation1()
        dragAnimation3()
        dragAnimation4()
        dragAnimation5()
        //maskViewForDraging.layer.removeAllAnimations()
    }
    
    override func btnSaveTouched() {
        super.btnSaveTouched()
        let alertView = UIAlertView(title: TutorialFinishedAlertViewTitleStr, message: TutorialFinishedAlertViewMessageStr, delegate: nil, cancelButtonTitle: TutorialFinishedAlertViewCancelButtonTitleStr)
        alertView.show()
    }
    
    //MARK:Event and State transfer
    override func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        super.imagePickerController(picker, didFinishPickingMediaWithInfo: info)
        //self.tutorialState = TutorialState.Draging
        switch self.typeOfAcquiredPhoto{
        case PhotoType.Scene:
            self.tutorialState = TutorialState.Draging
        case PhotoType.Item:
            self.tutorialState = TutorialState.SceneAdding
        default:
            SZLOG("unknown error")
        }
    }
    override func existedScenesPickerController(existedScenesPicker picker:ExistedScenesPickerController,didFinishPickingScene scene:SceneMO){
        super.existedScenesPickerController(existedScenesPicker: picker, didFinishPickingScene: scene)
        switch self.typeOfAcquiredPhoto{
        case PhotoType.Scene:
            self.tutorialState = TutorialState.Draging
        case PhotoType.Item:
            self.tutorialState = TutorialState.SceneAdding
        default:
            SZLOG("unknown error")
        }
    }
    override func saveCategoryAndDetail(popupWin: PopupWinType) {
        super.saveCategoryAndDetail(popupWin)
        self.tutorialState = TutorialState.Saving
        //self.label
    }
    
    //animations
    func indicatorAnimation(){
        
        SZLOG("2.LongPress - indicator")
        UIView.animateWithDuration(1, delay: 0.5, options: nil, animations: { () -> Void in
            self.longpress.transform = CGAffineTransformMakeScale(0.4, 0.4)
            }, completion: nil)
        
        SZLOG("Alpha - indicator")
        UIView.animateWithDuration(1, delay: 0.5, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
            self.longpress.alpha = 1
            }, completion: { (_) -> Void in
                UIView.animateWithDuration(0.5, delay: 1, options: nil, animations: { () -> Void in
                    self.longpress.alpha = 0
                }, completion: nil)
        })
        
        SZLOG("3.Drag - indicator")
        var deltaX = self.sceneView.center.x - self.itemView.center.x + 50
        var deltaY = self.itemView.center.y - self.sceneView.center.y - 30
        UIView.animateWithDuration(1, delay: 1.5, options: nil, animations: { () -> Void in
            self.longpress.center.x += deltaX
            self.longpress.center.y -= deltaY
            }, completion: nil)
        
        SZLOG("4.Lease - indicator")
        UIView.animateWithDuration(0.5, delay: 2.5, options: nil, animations: { () -> Void in
            self.longpress.transform = CGAffineTransformMakeScale(1, 1)
            }, completion: nil)
        
        SZLOG("5.MoveBack - indicator")
        UIView.animateWithDuration(0, delay: 3, options: nil, animations: { () -> Void in
            self.longpress.frame.origin.x = self.itemView.center.x - 40
            self.longpress.frame.origin.y = self.itemView.center.y - 55
            }, completion: nil)
    }
    
    func dragAnimation1(){
        SZLOG("1.Click - hand")
        UIView.animateWithDuration(0.5, delay: 0, options: nil, animations: { () -> Void in
            self.maskViewForDraging.transform = CGAffineTransformMakeScale(0.95, 0.95)
            self.maskViewForDraging.frame.origin.x -= 10
            self.maskViewForDraging.frame.origin.y -= 10
        }, completion: nil)
    }
    
    func dragAnimation3(){
        SZLOG("3.Drag - hand")
        var deltaX = self.sceneView.center.x - self.itemView.center.x + 50
        var deltaY = self.itemView.center.y - self.sceneView.center.y - 30
        UIView.animateWithDuration(1, delay: 1.5, options: nil, animations: { () -> Void in
            self.maskViewForDraging.center.x += deltaX
            self.maskViewForDraging.center.y -= deltaY
        }, completion: nil)
    }
    func dragAnimation4(){
        SZLOG("4.Lease - hand")
        UIView.animateWithDuration(0.5, delay: 2.5, options: nil, animations: { () -> Void in
            self.maskViewForDraging.transform = CGAffineTransformMakeScale(1, 1)
            self.maskViewForDraging.frame.origin.x += 10
            self.maskViewForDraging.frame.origin.y += 10
        }, completion: nil)
    }
    func dragAnimation5(){
        SZLOG("5.MoveBack - hand")
        UIView.animateWithDuration(0, delay: 3, options: nil, animations: { () -> Void in
            self.maskViewForDraging.frame.origin.x = self.itemView.center.x
            self.maskViewForDraging.frame.origin.y = self.itemView.center.y
        }, completion: nil)
    }
}
