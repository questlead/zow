//
//  ManagedObjects.swift
//  AlwaysForget
//
//  Created by User on 6/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import Foundation
import CoreData
//MARK: Item Managed Object -> Item Entity
@objc
class ItemMO:NSManagedObject{
    //MARK: Init And Deinit
    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    deinit{
    }
    //MARK: Properties
    @NSManaged var creationDate:NSDate
    @NSManaged var localIdentifier:String
    @NSManaged var detail:String?
    @NSManaged var positionX:Double
    @NSManaged var positionY:Double
    @NSManaged var frameWidthInScene:Double
    @NSManaged var frameHeightInScene:Double
    @NSManaged var data:NSData
    @NSManaged var containerScene: SceneMO?
    @NSManaged var relatedCategory: CategoryMO?
    //MARK: data model creation method with saving
    class func insertNewItem(creationDate date:NSDate,localIdentifier id:String!,imageData data:NSData) -> ItemMO{
        
        let item = NSEntityDescription.insertNewObjectForEntityForName("Item", inManagedObjectContext: appDelegate.managedObjectContext!) as! ItemMO
        item.creationDate = date
        item.localIdentifier = id
        item.data = data
        SZLOG(CategoryMO.fetchCategory(categoryName: defaultCatetory)?.name)
        item.relatedCategory = CategoryMO.fetchCategory(categoryName: "Unclass")
        SZLOG(item.relatedCategory?.name)
        //appDelegate.saveContext()
        return item
    }
    //MARK: place item on scene with saving
    class func placeItemOnScene(#item:ItemMO,scene:SceneMO,positionXInScene x:Double,positionYInScene y:Double,itemWidthInScene width:Double,itemHeightInScene height:Double,detail:String?,category:String?){
        item.positionX = x
        item.positionY = y
        item.frameWidthInScene = width
        item.frameHeightInScene = height
        item.containerScene = scene
        item.detail = detail
        item.relatedCategory = CategoryMO.fetchCategory(categoryName: category ?? "Unclass")
        SZLOG(item.relatedCategory?.name)
        SZLOG("")
        //appDelegate.saveContext()
    }
    class func cancelPlaceItemOnScene(#item:ItemMO){
        item.positionX = -1
        item.positionY = -1
        item.frameWidthInScene = 0
        item.frameHeightInScene = 0
        item.containerScene = nil
        item.detail = nil
        item.relatedCategory = CategoryMO.fetchCategory(categoryName:  "Unclass")
    }
    //MARK: delete an Item
    class func deleteItem(#item:ItemMO){
        appDelegate.managedObjectContext?.deleteObject(item)
        appDelegate.saveContext()
    }
    class func deleteItems(#items:[ItemMO]){
        for item in items{
            appDelegate.managedObjectContext?.deleteObject(item)
        }
        appDelegate.saveContext()
    }
    //MARK: fetch an Item with localIdentifier
    class func fetchItem(localIdentifier id:String) -> ItemMO?{
        let localIdentifierKey = "localIdentifier"
        let request:NSFetchRequest = NSFetchRequest(entityName: "Item")
        request.predicate = NSPredicate(format: "%K Like %@", argumentArray: [localIdentifierKey,id])
        let result = appDelegate.managedObjectContext!.executeFetchRequest(request, error: nil)
        SZLOG((result?.first as? ItemMO)?.localIdentifier)
        return result?.first as? ItemMO
    }
    class func fetchItems() -> [ItemMO]?{
        let request = NSFetchRequest(entityName: "Item")
        let results = appDelegate.managedObjectContext!.executeFetchRequest(request, error: nil)
        var sortedResults = results
        sortedResults?.sort({ (ele1, ele2) -> Bool in
            let dateComparison:NSComparisonResult = ((ele1 as! ItemMO).creationDate as NSDate).compare(((ele2 as! ItemMO).creationDate as NSDate))
            var sortFlag:Bool!
            switch dateComparison{
            case NSComparisonResult.OrderedAscending: sortFlag = true
            case NSComparisonResult.OrderedSame: sortFlag = true
            case NSComparisonResult.OrderedDescending: sortFlag = false
            default:
                sortFlag = false
                SZLOG("unknown error")
            }
            return sortFlag
        })
        return sortedResults as? [ItemMO]
    }
    class func fetchItems(searchedContext context:String) -> [ItemMO]?{
        let detailKey = "detail"
        let request:NSFetchRequest = NSFetchRequest(entityName: "Item")
        if context != ""{
            request.predicate = NSPredicate(format: "%K CONTAINS[cd] %@", argumentArray: [detailKey,context])
        }
        let results = appDelegate.managedObjectContext!.executeFetchRequest(request, error: nil)
        var sortedResults = results
        sortedResults?.sort({ (ele1, ele2) -> Bool in
            let dateComparison:NSComparisonResult = ((ele1 as! ItemMO).creationDate as NSDate).compare(((ele2 as! ItemMO).creationDate as NSDate))
            var sortFlag:Bool!
            switch dateComparison{
            case NSComparisonResult.OrderedAscending: sortFlag = true
            case NSComparisonResult.OrderedSame: sortFlag = true
            case NSComparisonResult.OrderedDescending: sortFlag = false
            default:
                sortFlag = false
                SZLOG("unknown error")
            }
            return sortFlag
        })
        return sortedResults as? [ItemMO]
    }
    class func fetchItems(searchedContext context:String,category:CategoryMO!) -> [ItemMO]?{
        let detailKey = "detail"
        let request:NSFetchRequest = NSFetchRequest(entityName: "Item")
        if context != ""{
            request.predicate = NSPredicate(format: "%K CONTAINS[cd] %@", argumentArray: [detailKey,context])
        }
        let results = appDelegate.managedObjectContext!.executeFetchRequest(request, error: nil)
        var resultsInCategory = results?.filter({(result) -> Bool in
            if (result as! ItemMO).relatedCategory == category {
                return true
            }else{
                return false
            }
        })
        var sortedResults = resultsInCategory
        sortedResults?.sort({ (ele1, ele2) -> Bool in
            let dateComparison:NSComparisonResult = ((ele1 as! ItemMO).creationDate as NSDate).compare(((ele2 as! ItemMO).creationDate as NSDate))
            var sortFlag:Bool!
            switch dateComparison{
            case NSComparisonResult.OrderedAscending: sortFlag = true
            case NSComparisonResult.OrderedSame: sortFlag = true
            case NSComparisonResult.OrderedDescending: sortFlag = false
            default:
                sortFlag = false
                SZLOG("unknown error")
            }
            return sortFlag
        })
        return sortedResults as? [ItemMO]
    }
}

//MARK: Scene Managed Object -> Scene Entity
@objc
class SceneMO:NSManagedObject {
    //MARK: Init And Deinit
    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    deinit{
    }
    //MARK: Properties
    @NSManaged var creationDate:NSDate
    @NSManaged var room:String?
    @NSManaged var localIdentifier:String
    @NSManaged var data:NSData
    @NSManaged var thumbnailData:NSData
    @NSManaged var childItems:Set<ItemMO>
    //MARK: data model creation method with saving
    class func insertNewScene(creationDate date:NSDate,localIdentifier id:String!,imageData data:NSData) -> SceneMO{
        let scene = NSEntityDescription.insertNewObjectForEntityForName("Scene", inManagedObjectContext: appDelegate.managedObjectContext!) as! SceneMO
        scene.creationDate = date
        scene.localIdentifier = id
        scene.data = data
        //let thumbnailImage = UIImage(data: data)!.thumbnailImage()
        //scene.thumbnailData = UIImagePNGRepresentation(thumbnailImage)
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
        dispatch_async(queue, { () -> Void in
            let thumbnailImage = UIImage(data: data)!.thumbnailImage()
            scene.thumbnailData = UIImagePNGRepresentation(thumbnailImage)
                })
        return scene
    }
    //MARK: delete a Scene
    class func deleteScene(#scene:SceneMO){
        appDelegate.managedObjectContext?.deleteObject(scene)
        //appDelegate.saveContext()
    }
    class func deleteEmptyScene(){
        if let allScenes = self.fetchScenes(){
            for scene in allScenes{
                if scene.childItems.count == 0 {
                    self.deleteScene(scene: scene)
                }
            }
        }
    }
    //MARK: fetch an Scene with localIdentifier
    class func fetchScene(localIdentifier id:String) -> SceneMO?{
        let localIdentifierKey = "localIdentifier"
        let request:NSFetchRequest = NSFetchRequest(entityName: "Scene")
        request.predicate = NSPredicate(format: "%K Like %@", argumentArray: [localIdentifierKey,id])
        let result = appDelegate.managedObjectContext!.executeFetchRequest(request, error: nil)
        SZLOG((result?.first as? SceneMO)?.localIdentifier)
        return result?.first as? SceneMO
    }
    class func fetchScenes() -> [SceneMO]?{
        let request = NSFetchRequest(entityName: "Scene")
        let results = appDelegate.managedObjectContext?.executeFetchRequest(request, error: nil)
        var sortedResults = results
        sortedResults?.sort({ (ele1, ele2) -> Bool in
            let dateComparison:NSComparisonResult = ((ele1 as! SceneMO).creationDate as NSDate).compare(((ele2 as! SceneMO).creationDate as NSDate))
            var sortFlag:Bool!
            switch dateComparison{
            case NSComparisonResult.OrderedAscending: sortFlag = true
            case NSComparisonResult.OrderedSame: sortFlag = true
            case NSComparisonResult.OrderedDescending: sortFlag = false
            default:
                sortFlag = false
                SZLOG("unknown error")
            }
            return sortFlag
        })
        return sortedResults as? [SceneMO]
    }
}

//MARK: Category Managed Object -> Category Entity
@objc
class CategoryMO:NSManagedObject{
    //MARK: Init And Deinit
    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    deinit{
    }
    //MARK: Properties
    @NSManaged var name:String
    @NSManaged var order:Int
    @NSManaged var itemsInCategory:Set<ItemMO>
    //MARK: data model creation method with saving
    class func insertNewCategory(categoryName name:String) -> CategoryMO{
        if let catMO = self.fetchCategory(categoryName: name){
            SZLOG("category already exists")
            return catMO
        }
        let category = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: appDelegate.managedObjectContext!) as! CategoryMO
        category.name = name
        

//        if let numOfCategories:Int = self.fetchCategories()?.count{
//            category.order = numOfCategories
//        }
        var existedCategoryies  = self.fetchCategories() ?? []
        existedCategoryies.sort { (cat1, cat2) -> Bool in
            return cat1.order < cat2.order
        }
        category.order = (existedCategoryies.last?.order ?? 0) + 1
        
        
        var notificationForInsertingNewCategory = NSNotification(name: "CategoryUpdated", object: category)
        notificationCenter.postNotification(notificationForInsertingNewCategory)
        
        return category
    }
    class func insertDefaultCategory(categoryName name:String) -> CategoryMO{
        if let catMO = self.fetchCategory(categoryName: name){
            SZLOG("category already exists")
            return catMO
        }
        let category = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: appDelegate.managedObjectContext!) as! CategoryMO
        category.name = name
        if let numOfCategories:Int = self.fetchCategories()?.count{
            //category.setValue(numOfCategories, forKey: "order")
            category.order = numOfCategories
        }
        return category
    }
    //MARK: delete a Category
    class func deleteCategory(#category:CategoryMO){
        for catKey in defaultCategoriesKeys{
            if category.name == catKey {
                SZLOG("cannot delete default category")
                return
            }
        }
        let items = ItemMO.fetchItems(searchedContext: "", category: category) ?? []
        for item in items{
            item.relatedCategory = CategoryMO.fetchCategory(categoryName: "Unclass")
        }
        
        appDelegate.managedObjectContext?.deleteObject(category)
        var notificationForInsertingNewCategory = NSNotification(name: "CategoryUpdated", object: category)
        notificationCenter.postNotification(notificationForInsertingNewCategory)
        //appDelegate.saveContext()
    }
    class func deleteCategory(categoryName name:String) {
        if let cat = self.fetchCategory(categoryName: name){
            self.deleteCategory(category: cat)
        }else{
            SZLOG("failed to delete category")
        }
    }
    //MARK: fetch Categories
    class func fetchCategories() ->[CategoryMO]?{
        let request:NSFetchRequest = NSFetchRequest(entityName: "Category")
        let results = appDelegate.managedObjectContext!.executeFetchRequest(request, error: nil)
        //SZLOG((results as? [CategoryMO])?.count)
        return results as? [CategoryMO]
    }
    class func fetchCategory(categoryName name:String) -> CategoryMO?{
        let nameKey = "name"
        let request = NSFetchRequest(entityName: "Category")
        request.predicate = NSPredicate(format: "%K LIKE %@", argumentArray: [nameKey,name])
        let result = appDelegate.managedObjectContext?.executeFetchRequest(request, error: nil)
        return result?.first as? CategoryMO
    }
}
