//
//  ItemCollectionViewCell.swift
//  AlwaysForget
//
//  Created by User on 19/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
let alertViewForDeletingItemTitle:String = NSLocalizedString("Delete item(s)?",comment: "Alert View For Deleting Item: Title")
class ItemCollectionViewCell: UICollectionViewCell,UIAlertViewDelegate {
    //MARK: Properties
    //related item
    var assoicatedItemMO:ItemMO!
    var imgView : UIImageView?
    lazy var deleteBtn: UIButton? = {
        var deleteBtn = UIButton(frame: CGRect(x: bgWidth/4 - 40, y: 0, width: 40, height: 40))
        //deleteBtn.backgroundColor = UIColor.redColor()
        //deleteBtn.layer.cornerRadius = deleteBtn.frame.height/2
        deleteBtn.setBackgroundImage(UIImage(named: "delete"), forState: UIControlState.Normal)
        deleteBtn.addTarget(self, action: "deleteBtnTouched", forControlEvents: UIControlEvents.TouchUpInside)
        deleteBtn.hidden = true
        return deleteBtn
    }()
    lazy var selectBtn: UIButton = {
        var selectBtn = UIButton(frame: CGRect(x: bgWidth/4 - 40, y: bgWidth/4 - 40, width: 40, height: 40))
        //selectBtn.backgroundColor = UIColor.lightGrayColor()
        //selectBtn.layer.cornerRadius = selectBtn.frame.height/2
        //selectBtn.layer.borderWidth = 1
        //selectBtn.layer.borderColor = UIColor.whiteColor().CGColor
        selectBtn.addTarget(self, action: "selectBtnTouched", forControlEvents: UIControlEvents.TouchUpInside)
        selectBtn.hidden = true
        selectBtn.userInteractionEnabled = false
        return selectBtn
    }()
    var isDeleteBtnHidden = true{
        willSet{
            if newValue == false{
                self.delegate?.enableTapGesture!()
            }else{
                self.delegate?.disenableTapGesture!()
            }
        }
    }
    var isSelectBtnSelected = false{
        willSet{
            if newValue == false{
                selectBtn.setBackgroundImage(UIImage(named: "select"), forState: UIControlState.Normal)
            }else{
                selectBtn.setBackgroundImage(UIImage(named: "selectSelected"), forState: UIControlState.Normal)
            }
        }
    }
    weak var delegate:ItemPickerControllerDelegate?

    //ges rec
    var longPressGestureRecognizer: UILongPressGestureRecognizer?

    //alertView for deleting scenes
    lazy var alertViewForDeletingItem:UIAlertView? = {
        return UIAlertView(title: alertViewForDeletingItemTitle, message: "", delegate: self, cancelButtonTitle: cancelStr, otherButtonTitles: popupOkBtnStr)
        }()
    //MARK:Init and Deinit
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //long press related
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPressGesture:")
        longPressGestureRecognizer!.numberOfTouchesRequired = 1
        longPressGestureRecognizer!.allowableMovement = 50
        longPressGestureRecognizer!.minimumPressDuration = 0.5
        //self.addGestureRecognizer(longPressGestureRecognizer!)
        
        imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: (1/4)*bgWidth - 1, height: (1/4)*bgWidth - 1))
        
        isDeleteBtnHidden = deleteBtn!.hidden
        
        self.addSubview(self.imgView!)
        self.addSubview(deleteBtn!)
        self.addSubview(selectBtn)
        
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    deinit {
        
    }
    //MARK:Gesture Recognizer
    func handleLongPressGesture(recognizer:UILongPressGestureRecognizer){
        if (recognizer.state == UIGestureRecognizerState.Began){
            SZLOG("long press")
            deleteBtn?.hidden = false
            isDeleteBtnHidden = deleteBtn!.hidden
        }
    }


    //MARK: delete btn and related alert view
    func deleteBtnTouched(){
        self.alertViewForDeletingItem?.show()
    }
    
    func selectBtnTouched(){
        self.isSelectBtnSelected = !self.isSelectBtnSelected
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        switch buttonIndex{
        case 0:
            SZLOG("cancel button click")
            deleteBtn?.hidden = true
            isDeleteBtnHidden = deleteBtn!.hidden
        case 1:
            SZLOG("confirm button click")
            deleteBtn?.hidden = true
            isDeleteBtnHidden = deleteBtn!.hidden
            ItemMO.deleteItem(item: self.assoicatedItemMO)
            let notificationForDeletingAItem = NSNotification(name: "DeleteItem", object: self.assoicatedItemMO)
            notificationCenter.postNotification(notificationForDeletingAItem)
        default:
            SZLOG("unkown error")
        }
    }

    
}
