//
//  AppDelegate.swift
//  AlwaysForget
//
//  Created by User on 6/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
import CoreData
import Parse
//MARK:appDelegate Obj
let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//MARK:NSNotificationCenter
let notificationCenter = NSNotificationCenter.defaultCenter()
//LocalizedStringConstants
let appName = NSLocalizedString("ZOW",comment: "app name: ZOW")
let appID:Int = 1035113622 //default is lazy money book
let UMappID:String = "55e18b60e0f55a0cb1000e4b"
let UMShareText:String = "这是小玩意儿，帮您简单记录、灵活查找、准确定位您的任何物品。https://itunes.apple.com/au/app/ZOW/id\(appID)?mt=8"
let RatingLinkStr:String = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=\(appID)&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"

let sceneStr = NSLocalizedString("Set a SCENE to place your item",comment: "Scene")
let itemStr = NSLocalizedString("Item",comment: "Item")
let defaultCatetory = NSLocalizedString("Unclass",comment: "Default Catetory 0")
let defaultCatetoryCloth = NSLocalizedString("Cloth",comment: "Default Catetory 1")
let defaultCatetoryMisc = NSLocalizedString("Misc",comment: "Default Catetory 2")
let defaultCatetoryJewel = NSLocalizedString("Jewel",comment: "Default Catetory 3")
let defaultCatetoryBook = NSLocalizedString("Book",comment: "Default Catetory 4")
let defaultCatetoryCerti = NSLocalizedString("Certi",comment: "Default Catetory 5")
let defaultCatetoryBag = NSLocalizedString("Bag", comment: "Default Catetory 6")
let defaultCatetoryShoes = NSLocalizedString("Shoes", comment: "Default Catetory 7")
let defaultCatetoryMakeup = NSLocalizedString("Makeup", comment: "Default Catetory 8")
let defaultCatetorySport = NSLocalizedString("Sport", comment: "Default Catetory 9")
let defaultCatetoryDigit = NSLocalizedString("Digit", comment: "Default Catetory 10")
let defaultCatetoryReceipt = NSLocalizedString("Receipt", comment: "Default Catetory 11")
let defaultCatetoryMed = NSLocalizedString("Med", comment: "Default Catetory 12")
let defaultCatetoryCD = NSLocalizedString("CD", comment: "Default Catetory 13")
let defaultCatetoryAll = NSLocalizedString("All", comment: "Default Catetory 14")
let defaultCategoriesKeys = ["Unclass","Cloth","Misc","Jewel","Certi","Book","Shoes","Makeup","Sport","Digit","Receipt","Med","CD","All"]
//MARK: AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    // categories MO
    var categories:[CategoryMO] {
        get{
            var categoriesTemp:[CategoryMO]
            if CategoryMO.fetchCategories()?.count != 0{
                categoriesTemp = CategoryMO.fetchCategories()!
            }else{
                CategoryMO.insertDefaultCategory(categoryName: "Cloth")
                CategoryMO.insertDefaultCategory(categoryName: "Misc")
                CategoryMO.insertDefaultCategory(categoryName: "Jewel")
                CategoryMO.insertDefaultCategory(categoryName: "Book")
                CategoryMO.insertDefaultCategory(categoryName: "Certi")
                CategoryMO.insertDefaultCategory(categoryName: "Bag")
                CategoryMO.insertDefaultCategory(categoryName: "Shoes")
                CategoryMO.insertDefaultCategory(categoryName: "Makeup")
                CategoryMO.insertDefaultCategory(categoryName: "Sport")
                CategoryMO.insertDefaultCategory(categoryName: "Digit")
                CategoryMO.insertDefaultCategory(categoryName: "Receipt")
                CategoryMO.insertDefaultCategory(categoryName: "Med")
                CategoryMO.insertDefaultCategory(categoryName: "CD")
                CategoryMO.insertDefaultCategory(categoryName: "Unclass")
                categoriesTemp = CategoryMO.fetchCategories()!
                self.saveContext()
            }
            categoriesTemp.sort({ (cat1, cat2) -> Bool in
                return (cat1 as CategoryMO).order < (cat2 as CategoryMO).order
            })
            return categoriesTemp
        }
        set{
            SZLOG("set categories a new value")
        }
    }
    func updateCategories(){
        self.categories = CategoryMO.fetchCategories()!
    }
    // inin MO
    lazy var initMO:NSManagedObject = {
        let request = NSFetchRequest(entityName: "Initialization")
        let result = self.managedObjectContext!.executeFetchRequest(request, error: nil) as? [NSManagedObject]
        if result?.count == 1{
            return result!.first!
        }else{
            //let initObjTemp = self.managedObjectContext?.in
            return NSEntityDescription.insertNewObjectForEntityForName("Initialization", inManagedObjectContext: self.managedObjectContext!) as! NSManagedObject
        }
        }()
    // Log in MO
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        // init app
        //Parse API Activate and tracking
        Parse.enableLocalDatastore()
        Parse.setApplicationId("I9J8zstggH0zzxn2yKRC6gpFtwdbVmT1GPJYO073",
            clientKey: "aaMBC1SFTPv8ReI8uOvOhqpCwRKZ6la9lAjXY8wG")
        PFAnalytics.trackAppOpenedWithLaunchOptionsInBackground(nil, block: nil) // tracking launuch
        self.initialization()
        
        //login check
        self.login()
        
        notificationCenter.addObserver(self, selector: "updateCategories", name: "CategoryUpdated", object: nil)
        
        //UMService Related
        UMSocialData.setAppKey("UMappID")
        let urlStr:String = "http://www.umeng.com/social"
        let url:NSURL
        UMSocialConfig.hiddenNotInstallPlatforms([UMShareToQQ,UMShareToQzone,UMShareToWechatSession,UMShareToWechatTimeline])
        return true
    }
    
    func initialization(){
        if self.initMO.valueForKey("isFirstRunning") as! Bool == true{
            self.initMO.setValue(false, forKey: "isFirstRunning")
            self.saveContext()
            SZLOG("First time to run: true")
            (self.window!.rootViewController as! UINavigationController).pushViewController(TutorialHomeViewController(), animated: false)
        }else{
            SZLOG("First time to run: false")
        }
    }
    func login(){
        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        notificationCenter.removeObserver(self)
        //self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "Silversr.AlwaysForget" in the application's documents Application Support directory.
        
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as! NSURL
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        var modelURL:NSURL!
        SZLOG(deviceName())
        //@"iPhone4,1" on iPhone 4S
        //@"iPhone5,1" on iPhone 5 (model A1428, AT&T/Canada)
        //@"iPhone5,2" on iPhone 5 (model A1429, everything else)
        //@"iPhone5,3" on iPhone 5c (model A1456, A1532 | GSM)
        //@"iPhone5,4" on iPhone 5c (model A1507, A1516, A1526 (China), A1529 | Global)
        //@"iPhone6,1" on iPhone 5s (model A1433, A1533 | GSM)
        //@"iPhone6,2" on iPhone 5s (model A1457, A1518, A1528 (China), A1530 | Global)
        //@"iPhone7,1" on iPhone 6 Plus
        //@"iPhone7,2" on iPhone 6
        switch deviceName(){
        case "iPhone6,1","iPhone6,2","iPhone7,1","iPhone7,2":
            SZLOG("64 bit iPhone")
            modelURL = NSBundle.mainBundle().URLForResource("AlwaysForget", withExtension: "momd")!
        case "iPhone5,1","iPhone5,2","iPhone5,3","iPhone5,4","iPhone4,1":
            modelURL = NSBundle.mainBundle().URLForResource("AlwaysForget32BitMachine", withExtension: "momd")!
            SZLOG("32 bit iPhone")
        case "x86_64":
            modelURL = NSBundle.mainBundle().URLForResource("AlwaysForget", withExtension: "momd")!
            SZLOG("64 bit Simulator")
        default:
            modelURL = NSBundle.mainBundle().URLForResource("AlwaysForget", withExtension: "momd")!
            SZLOG("Others, maybe iPad. 64 bit assumption")
        }
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("AlwaysForget.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        managedObjectContext.undoManager = NSUndoManager()
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }
    
}

