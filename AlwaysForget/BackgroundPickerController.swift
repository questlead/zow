//
//  Background.swift
//  AlwaysForget
//
//  Created by User on 10/08/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
public var BackgroundImageStr:String = NSUserDefaults.standardUserDefaults().stringForKey("BackgroundImageStr") ?? "bg"
class BackgroundPickerController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: Properties

    lazy var naviBarView:UIView = {
        let naviBarView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: 64))
        naviBarView.backgroundColor = UIColor.myBlueColor()
        return naviBarView
        }()
    lazy var naviTitleLable : UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = BackgroundStr
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(20)
        return naviTitleLable
        }()
    lazy private var backgroundCollectionView:UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: bgWidth, height: bgHeight - 64), collectionViewLayout: self.layout)
        collectionView.backgroundColor = UIColor.grayColor()
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
        }()
    lazy private var layout:UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: (1/2)*bgWidth - 1, height: (1/2)*bgHeight - 1 - (1/2)*64)
        layout.minimumInteritemSpacing = CGFloat(2)
        layout.minimumLineSpacing = CGFloat(2)
        layout.scrollDirection = UICollectionViewScrollDirection.Vertical
        return layout
        }()
    
    //MARK: init and deinit
//    init(){
//        
//    }
//    required init(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
//        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)s
//    }
    deinit{
//        for i in 0...3{
//            (self.backgroundCollectionView.cellForItemAtIndexPath(NSIndexPath(forRow: Int(i), inSection: 0)) as! BackgroundPickingCollectionViewCell).imageView = nil
//        }
        //self.backgroundCollectionView = UICollectionView()
        SZLOG("deinit")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UIViewControllerDelegate and related func
    override func viewWillAppear(animated: Bool) {
    }
    override func viewWillDisappear(animated: Bool) {
    }
    override func viewDidLoad() {
        SZLOG("init and load")
        super.viewDidLoad()
        self.view.addSubview(naviBarView)
        self.view.addSubview(naviTitleLable)
        self.view.addSubview(self.backgroundCollectionView)
        
    }

    
    //MARK: UICollectionViewDelegate and DataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        collectionView.registerClass(BackgroundPickingCollectionViewCell.self, forCellWithReuseIdentifier: "BackgroundCellID")
        var cell: BackgroundPickingCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("BackgroundCellID", forIndexPath: indexPath) as! BackgroundPickingCollectionViewCell
        switch indexPath.item{
        case 0:
            cell.imageView!.image = UIImage(named: "bg")
            cell.backgroundColor = UIColor.greenColor()
        case 1:
            cell.imageView!.image = UIImage(named: "bg2")
            cell.backgroundColor = UIColor.grayColor()
        case 2:
            cell.imageView!.image = UIImage(named: "bg3")
            cell.backgroundColor = UIColor.yellowColor()
        case 3:
            cell.imageView!.image = UIImage(named: "bg4")
            cell.backgroundColor = UIColor.purpleColor()
        case 4:
            cell.imageView!.image = UIImage(named: "bg5")
            cell.backgroundColor = UIColor.purpleColor()
        case 5:
            cell.imageView!.image = UIImage(named: "bg6")
            cell.backgroundColor = UIColor.purpleColor()
        case 6:
            cell.imageView!.image = UIImage(named: "bg7")
            cell.backgroundColor = UIColor.purpleColor()
        case 7:
            cell.imageView!.image = UIImage(named: "bg8")
            cell.backgroundColor = UIColor.purpleColor()
        case 8:
            cell.imageView!.image = UIImage(named: "bg9")
            cell.backgroundColor = UIColor.purpleColor()
        case 9:
            cell.imageView!.image = UIImage(named: "bg10")
            cell.backgroundColor = UIColor.purpleColor()
//        case 10:
//            cell.imageView!.image = UIImage(named: "bg11")
//            cell.backgroundColor = UIColor.purpleColor()
        default:
            SZLOG("unknown error")
        }
        return cell
        
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        switch indexPath.item{
        case 0: BackgroundImageStr = "bg"
        case 1: BackgroundImageStr = "bg2"
        case 2: BackgroundImageStr = "bg3"
        case 3: BackgroundImageStr = "bg4"
        case 4: BackgroundImageStr = "bg5"
        case 5: BackgroundImageStr = "bg6"
        case 6: BackgroundImageStr = "bg7"
        case 7: BackgroundImageStr = "bg8"
        case 8: BackgroundImageStr = "bg9"
        case 9: BackgroundImageStr = "bg10"
        //case 10: BackgroundImageStr = "bg11"
        default:
            SZLOG("unknown error")
        }
        NSUserDefaults.standardUserDefaults().setObject(BackgroundImageStr, forKey: "BackgroundImageStr")
        NSUserDefaults.standardUserDefaults().synchronize()
        self.navigationController?.popToRootViewControllerAnimated(true)
        (self.navigationController?.visibleViewController as! HomeViewController).bgImage.image = UIImage(named: BackgroundImageStr)
    }
}
class BackgroundPickingCollectionViewCell:UICollectionViewCell{
    
    //MARK:Properties
    var imageView:UIImageView?
    
    //MARK:Init and deinit
    override init(frame: CGRect) {
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (1/2)*bgWidth - 1, height: (1/2)*bgHeight - 1 - (1/2)*64))
        super.init(frame: frame)
        self.addSubview(self.imageView!
        )
    }
    deinit{
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
