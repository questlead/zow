//
//  DeviceName.m
//  AlwaysForget
//
//  Created by User on 28/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//


#import "DeviceName.h"
NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}