//
//  ManagerAddNewCell.swift
//  AlwaysForget
//
//  Created by User on 29/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

class ManagerAddNewCell: UICollectionViewCell {

    var smallAddNewImg: UIImageView?
    var largeAddNewImg: UIImageView?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.myOpaqueBlueColor()
        smallAddNewImg = UIImageView(frame: CGRect(x: 25, y: 25, width: frame.width - 50, height: frame.height - 50))
        //smallAddNewImg!.backgroundColor = UIColor.lightGrayColor()
        smallAddNewImg!.image = UIImage(named: "AddNewScene")
        smallAddNewImg!.layer.shadowOffset = CGSizeMake(0, 0)
        smallAddNewImg!.layer.shadowColor = UIColor.myBrownColor().CGColor
        smallAddNewImg!.layer.shadowOpacity = 0.1
        //smallAddNewImg!.hidden = true
        
//        largeAddNewImg = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
//        largeAddNewImg!.backgroundColor = UIColor.lightGrayColor()
//        largeAddNewImg!.image = UIImage(named: "AddNewScene")
//        largeAddNewImg!.hidden = true
        
        self.addSubview(smallAddNewImg!)
//        self.addSubview(largeAddNewImg!)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
