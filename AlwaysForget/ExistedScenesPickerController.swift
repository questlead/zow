//
//  ExistedScenesViewController.swift
//  AlwaysForget
//
//  Created by User on 18/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

//private let alertViewForDeletingSceneTitle = NSLocalizedString("Confirm To Delete",comment: "confirm to delete scenes")
private let alertViewForDeletingSceneMessage:String = NSLocalizedString("Deleting this scene will result in all item related to this scene deleted",comment: "Alert View For Deleting Scene: Message")
@objc protocol ExistedScenesPickerControllerDelegate{
    optional func existedScenesPickerController(existedScenesPicker picker:ExistedScenesPickerController,didFinishPickingScene scene:SceneMO)
    optional func existedScenesPickerControllerDidCancel(existedScenesPicker picker:ExistedScenesPickerController)
    optional func enableTapGesture()
    optional func disenableTapGesture()
}
//MARK: UIImage to get thumbnail image and cropped image
extension UIImage{
    func croppedImage(imageSizeWidth width:CGFloat = 500,imageSizeHeight height:CGFloat = 500) -> UIImage{
        let size = CGSizeMake(width, height)
        let scale:CGFloat = max(size.width/self.size.width, size.height/self.size.height)
        let scaledWidth:CGFloat = self.size.width * scale;
        let scaledHeight:CGFloat = self.size.height * scale;
        let imageRect:CGRect = CGRectMake((size.width - scaledWidth)/CGFloat(2.0),(size.height - scaledHeight)/CGFloat(2.0),scaledWidth, scaledHeight);
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.drawInRect(imageRect)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage;
    }
    func thumbnailImage() -> UIImage{
        return self.croppedImage(imageSizeWidth: 320, imageSizeHeight: 320)
    }
}
@objc protocol Deletable{
    var selecting:Bool {get set}
    var toBeDeletedCellIndexPaths:[NSIndexPath] {get set}
    optional var toBeDeletedScenes:[SceneMO] {get set}
    optional var toBeDeletedItems:[ItemMO] {get set}
    optional var toBeDeletedCategories:[CategoryMO] {get set}
    //func deleteCells(toBeDeletedCellIndexPaths indexPath:NSIndexPath,isSelectingState state:Bool)
    func deleteCells()
}
class ExistedScenesPickerController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,ImageQualitySliderDelegate,ExistedScenesPickerControllerDelegate,Deletable{

    //MARK: Properties
    var selecting:Bool = false
    var toBeDeletedCellIndexPaths:[NSIndexPath] = []
    var toBeDeletedScenes:[SceneMO] = []
    var isPushFromSettings:Bool = false
    
    private var existedScenes:[SceneMO] = SceneMO.fetchScenes() ?? []
    
    weak var delegate:ExistedScenesPickerControllerDelegate?
    
    lazy var naviBarView:UIView = {
        let naviBarView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: 64))
        naviBarView.backgroundColor = UIColor.myBlueColor()
        return naviBarView
        }()
    lazy var naviTitleLable : UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = ScenesStr
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(20)
        return naviTitleLable
        }()
    lazy private var existedScenesCollectionView:UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: bgWidth, height: bgHeight - 64), collectionViewLayout: self.layout)
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
        }()
    lazy private var layout:UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: bgWidth/2 - 1, height: (1/2)*bgWidth - 1)
        layout.minimumInteritemSpacing = CGFloat(2)
        layout.minimumLineSpacing = CGFloat(2)
        layout.scrollDirection = UICollectionViewScrollDirection.Vertical
        return layout
        }()
    lazy private var photoAcquiringActionSheet:UIActionSheet = {
        let actionSheet = UIActionSheet(title: phtotAcquiringActionSheetTitle, delegate: self, cancelButtonTitle: photoAcquiringActionSheetCancelButtonTitle, destructiveButtonTitle: nil)
        actionSheet.addButtonWithTitle(photoAcquiringActionSheetPhotoLibraryButtonTitle)
        actionSheet.addButtonWithTitle(photoAcquiringActionSheetCameraButtonTitle)
        return actionSheet
        }()
    lazy private var imagePickerControllerFromCamera:UIImagePickerController = {
        let imgPicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            imgPicker.sourceType = UIImagePickerControllerSourceType.Camera
            if UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)?.isEmpty == false{
                imgPicker.mediaTypes = [UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)![0]]
                SZLOG(imgPicker.mediaTypes)
                //imgPicker.cameraDevice = UIImagePickerControllerCameraDevice.Front
                
                //customize camera to generate squire picture
                imgPicker.showsCameraControls = true
                //let windowsSize = self.view.window?.bounds
                let CameraControlView = CameraOverlayView(frame: CGRect(x: 0, y: bgHeight - 120, width: bgWidth, height: 50))
                imgPicker.cameraOverlayView = CameraControlView
            }
        }
        imgPicker.delegate = self
        (imgPicker.cameraOverlayView as? CameraOverlayView)?.delegate = self
        return imgPicker
        }()
    lazy private var imagePickerControllerFromPhotoLibrary:UIImagePickerController = {
        let imgPicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            imgPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            if UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)?.isEmpty == false{
                imgPicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)!
                SZLOG(imgPicker.mediaTypes)
            }
        }
        imgPicker.delegate = self
        return imgPicker
        }()
    
    private var pickedImageQualityFromCamera:CGFloat = 0.5 // 0 - 1
    private var didSelectScene:Bool = false
    var tapGestureRecognizer: UITapGestureRecognizer?   
    
    lazy var naviSelect : UIBarButtonItem = {
        let naviSelect = UIBarButtonItem(title: selectStr, style: UIBarButtonItemStyle.Plain, target: self, action: "btnNaviSelectTouched")
        return naviSelect
        }()
    lazy var naviBin : UIButton = {
        let naviBin = UIButton(frame: CGRect(x: bgWidth - 110, y: 0, width: 44, height: 44))
        naviBin.setBackgroundImage(UIImage(named: "binSelected"), forState: UIControlState.Normal)
        naviBin.setBackgroundImage(UIImage(named: "bin"), forState: UIControlState.Highlighted)
        naviBin.addTarget(self, action: "btnBinTouched", forControlEvents: UIControlEvents.TouchUpInside)
        naviBin.hidden = true
        return naviBin
        }()
    var isNaviSelectBtnSelected = false{
        willSet{
            if newValue == true{
                naviSelect.title = "Cancel"
                naviBin.hidden = false
            }else{
                naviSelect.title = "Select"
                naviBin.hidden = true
            }
        }
    }
    //alertView for deleting scenes
    lazy var alertViewForDeletingItem:UIAlertView? = {
        return UIAlertView(title: alertViewForDeletingSceneTitle, message: alertViewForDeletingSceneMessage, delegate: self, cancelButtonTitle: cancelStr, otherButtonTitles: popupOkBtnStr)
        }()
    
    //MARK: init and deinit
    deinit{
        if self.didSelectScene == false {
            self.delegate?.existedScenesPickerControllerDidCancel?(existedScenesPicker: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: UIViewControllerDelegate and related func
    override func viewWillAppear(animated: Bool) {
        notificationCenter.addObserver(self, selector: "updateCollectionView", name: "DeleteScene", object: nil)
        self.navigationController?.navigationBar.addSubview(naviBin)
    }
    override func viewWillDisappear(animated: Bool) {
        notificationCenter.removeObserver(self)
        naviBin.removeFromSuperview()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.myWhiteColor()

        self.view.addSubview(naviBarView)
        self.view.addSubview(naviTitleLable)
        self.navigationItem.rightBarButtonItem = naviSelect
        self.navigationController?.navigationBar.addSubview(naviBin)
        self.view.addSubview(self.existedScenesCollectionView)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGesture")
        //existedScenesCollectionView.addGestureRecognizer(tapGestureRecognizer!)
        tapGestureRecognizer?.enabled = false
        
    }
    
    //MARK: Utilities
    func handleTapGesture(){
        if existedScenes.count != 0{
            for i in 0...existedScenes.count - 1{
                var cell = existedScenesCollectionView.cellForItemAtIndexPath(NSIndexPath(forItem: i, inSection: 0)) as? ExistedScenesCollectionViewCell
                cell?.deleteBtn?.hidden = true
                cell?.isDeleteBtnHidden = true
            }
        }
    }
    func updateCollectionView(){
        self.existedScenes = SceneMO.fetchScenes() ?? []
        appDelegate.saveContext()
        self.existedScenesCollectionView.reloadData()
    }
    
    func btnNaviSelectTouched(){
        if self.selecting == true{
            self.selecting = false
            //self.existedScenesCollectionView.allowsMultipleSelection = false
            self.existedScenesCollectionView.reloadData()
            toBeDeletedScenes = []
            toBeDeletedCellIndexPaths = []
        }else{
            self.selecting = true
            //self.existedScenesCollectionView.allowsMultipleSelection = true
            self.existedScenesCollectionView.reloadData()
        }
        isNaviSelectBtnSelected = !isNaviSelectBtnSelected
    }
    //MARK:Deletable
    func deleteCells(){
        SZLOG(self.toBeDeletedCellIndexPaths)
        for scene in self.toBeDeletedScenes{
            SceneMO.deleteScene(scene: scene)
        }
        self.updateSceneCollectionView()
    }
    func btnBinTouched(){
        self.alertViewForDeletingItem?.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        switch buttonIndex {
        case 0:
            SZLOG("cancel button click")
            existedScenesCollectionView.reloadData()
        case 1:
            SZLOG("confirm button click")
            SZLOG(self.existedScenes.count)
            self.deleteCells()
            //self.isNaviSelectBtnSelected = false
            toBeDeletedScenes = []
            toBeDeletedCellIndexPaths = []
            self.existedScenesCollectionView.reloadData()
        default:
            SZLOG("unkown error")
        }
    }
    
    func updateSceneCollectionView(){
        self.existedScenes = SceneMO.fetchScenes() ?? []
        appDelegate.saveContext()
        self.existedScenesCollectionView.reloadData()
    }

    
    
    //MARK: ExistedScenesPickerControllerDelegate
    func enableTapGesture() {
        tapGestureRecognizer?.enabled = true
    }
    func disenableTapGesture() {
        tapGestureRecognizer?.enabled = false
    }

    
    //MARK: UICollectionViewDelegate and DataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        SZLOG(existedScenes.count)
        return existedScenes.count + 1
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        collectionView.registerClass(ExistedScenesCollectionViewCell.self, forCellWithReuseIdentifier: "SceneCellID")
        collectionView.registerClass(ManagerAddNewCell.self, forCellWithReuseIdentifier: "AddNewCell")
        
        if indexPath.item == existedScenes.count{
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddNewCell", forIndexPath: indexPath) as! ManagerAddNewCell
            return cell
        }else{
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("SceneCellID", forIndexPath: indexPath) as! ExistedScenesCollectionViewCell
            cell.delegate = self
            cell.assoicatedSceneMO = existedScenes[indexPath.item]
            cell.imgView?.image = UIImage(data:  existedScenes[indexPath.item].thumbnailData)
            
            if self.selecting == true {
                cell.selectBtn.hidden = false
                cell.isSelectBtnSelected = false
            }else{
                cell.selectBtn.hidden = true
            }
            
            for index in toBeDeletedCellIndexPaths{
                if index == indexPath{
                    cell.isSelectBtnSelected = true
                    break
                }else{
                    cell.isSelectBtnSelected = false
                }
            }
            return cell
        }
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        //select last item for add new
        SZLOG(indexPath.item)
        if indexPath.item == existedScenes.count{
            if self.selecting == false {
                self.photoAcquiringActionSheet.showInView(self.view)
            }
        }else{
            self.didSelectScene = true
            let selectedCell:ExistedScenesCollectionViewCell = self.existedScenesCollectionView.cellForItemAtIndexPath(indexPath) as! ExistedScenesCollectionViewCell
            self.delegate?.existedScenesPickerController?(existedScenesPicker: self, didFinishPickingScene: selectedCell.assoicatedSceneMO)
            if isPushFromSettings == false{
                self.navigationController?.popViewControllerAnimated(true)
            }
            if self.selecting == true{
                selectedCell.isSelectBtnSelected = !selectedCell.isSelectBtnSelected
                if selectedCell.isSelectBtnSelected{
                    self.toBeDeletedCellIndexPaths.append(indexPath)
                    self.toBeDeletedScenes.append((collectionView.cellForItemAtIndexPath(indexPath) as! ExistedScenesCollectionViewCell).assoicatedSceneMO)
                }else{
                    var i = 0
                    var indexInArray = 0
                    if self.toBeDeletedCellIndexPaths.filter({ (ele) -> Bool in
                        if ele == indexPath{
                            indexInArray = i
                            i++
                            return true
                        }
                        i++
                        return false
                    }).count > 0{
                        self.toBeDeletedCellIndexPaths.removeAtIndex(indexInArray)
                        self.toBeDeletedScenes.removeAtIndex(indexInArray)
                    }
                }

            }
        }
    }
    //MARK: Action Sheet delegate and related imagePicker delegate
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int){
        SZLOG("clicked button at index")
        switch buttonIndex{
        case 0: SZLOG("Cancel Button Click")
        case 2: self.showCamera()
        case 1: self.showPhotoLibrary()
        default:
            SZLOG("unknown error")
        }
    }
    func actionSheetCancel(actionSheet: UIActionSheet){
        SZLOG("cancel action sheet")
    }
    func showPhotoLibrary(){
        presentViewController(self.imagePickerControllerFromPhotoLibrary, animated: true, completion: { () in
            SZLOG("Did show photo library")
        })
    }
    func showCamera(){
        presentViewController(self.imagePickerControllerFromCamera, animated: true, completion: { () in
            SZLOG("Did show Camera")
        })
    }
    func imageQualitySliderValueChanged(sliderValue value:Float){
        self.pickedImageQualityFromCamera = CGFloat(value)
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]){
        SZLOG("Did finish picking media with info")
        var imageData: NSData?
        var image:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //image = cropImage(image, choppedSize: CGSizeMake(100, 100))
        //SZLOG(image.size.width)
        
        let now = NSDate()
        imageData = UIImageJPEGRepresentation(image, self.pickedImageQualityFromCamera)
        SceneMO.insertNewScene(creationDate: now, localIdentifier: now.description, imageData: imageData!)
//        SceneMO.insertNewScene(creationDate: now, localIdentifier: now.description, imageData: imageData!, thumbnailImageData: <#NSData#>)
        dismissViewControllerAnimated(true, completion: { () in
            SZLOG("Did dismiss ImagePicker controller")
            self.existedScenes = SceneMO.fetchScenes() ?? []
            SZLOG(self.existedScenes.count)
            self.existedScenesCollectionView.reloadData()
            appDelegate.saveContext()
        })
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        SZLOG("Did cancel picking media")
        dismissViewControllerAnimated(true, completion: { () in
            SZLOG("Did dismiss ImagePicker controller")
        })
    }
}

