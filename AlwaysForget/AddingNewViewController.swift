//
//  AddingNewViewController.swift
//  AlwaysForget
//
//  Created by User on 6/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//
// 备注：拖拽手势，除了按钮以外的其他视图都要先enabel user activity再添加手势对象

import UIKit
import CoreData

//LocalizedString
let phtotAcquiringActionSheetTitle = NSLocalizedString("Please choose photo acquiring method",comment: "Photo Acquiring Action Sheet Title")
let photoAcquiringActionSheetCancelButtonTitle = NSLocalizedString("Cancel",comment: "Photo Acquiring Action Sheet Cancel Button Title")
let photoAcquiringActionSheetPhotoLibraryButtonTitle = NSLocalizedString("Photo Library",comment: "Photo Acquiring Action Sheet Photo Library Button Title")
let photoAcquiringActionSheetCameraButtonTitle = NSLocalizedString("Camera",comment: "Photo Acquiring Action Sheet Camera Button Title")
let photoAcquiringActionSheetExistedScenesButtonTitle = NSLocalizedString("Existed Scenes",comment: "Photo Acquiring Action Sheet Existed Scenes Button Title")
let alertViewTitle = NSLocalizedString("Incomplete Info",comment: "AlertView Title")
let alertViewMsg   = NSLocalizedString("You can't save record without scene/item/link!",comment: "AlertView Message")
let cancelStr = NSLocalizedString("Cancel",comment: "Cancel button on nav bar/ AlertView cancel button")
private let saveStr = NSLocalizedString("Save",comment: "Save button on nav bar")
private let beautifyStr = NSLocalizedString("Beautify",comment: "Beautify button")
private let detailStr = NSLocalizedString("Detail",comment: "Detail info edit")
private let moreStr = NSLocalizedString("More",comment: "More button")
private let categoryStr = NSLocalizedString("Category",comment: "Category button")

let emptyStr = ""

//MARK: Photo Type Enum
enum PhotoType:Int{
    case Item
    case Scene
}


class AddingNewViewController: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ExistedScenesPickerControllerDelegate,FPPopoverControllerDelegate,ImageQualitySliderDelegate {

    var typeOfAcquiredPhoto:PhotoType = PhotoType.Item //default item
    
    // item position in Scene
    private var positionX:Double = 0
    private var positionY:Double = 0
    
    lazy private var photoAcquiringActionSheetForItem:UIActionSheet = {
        let actionSheet = UIActionSheet(title: phtotAcquiringActionSheetTitle, delegate: self, cancelButtonTitle: photoAcquiringActionSheetCancelButtonTitle, destructiveButtonTitle: nil)
        actionSheet.addButtonWithTitle(photoAcquiringActionSheetPhotoLibraryButtonTitle)
        actionSheet.addButtonWithTitle(photoAcquiringActionSheetCameraButtonTitle)
        return actionSheet
        }()
    lazy private var photoAcquiringActionSheetForScene:UIActionSheet = {
        let actionSheet = UIActionSheet(title: phtotAcquiringActionSheetTitle, delegate: self, cancelButtonTitle: photoAcquiringActionSheetCancelButtonTitle, destructiveButtonTitle: nil)
        actionSheet.addButtonWithTitle(photoAcquiringActionSheetPhotoLibraryButtonTitle)
        actionSheet.addButtonWithTitle(photoAcquiringActionSheetCameraButtonTitle)
        actionSheet.addButtonWithTitle(photoAcquiringActionSheetExistedScenesButtonTitle)
        return actionSheet
        }()
    lazy private var imagePickerControllerFromCamera:UIImagePickerController = {
        let imgPicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            imgPicker.sourceType = UIImagePickerControllerSourceType.Camera
            if UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)?.isEmpty == false{
                imgPicker.mediaTypes = [UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)![0]]
                SZLOG(imgPicker.mediaTypes)
                //imgPicker.cameraDevice = UIImagePickerControllerCameraDevice.Front
                //imgPicker.allowsEditing = true
                //customize camera to generate squire picture
                imgPicker.showsCameraControls = true
                let CameraControlView = CameraOverlayView(frame: CGRect(x: 0, y: bgHeight - 120, width: bgWidth, height: 50))
                imgPicker.cameraOverlayView = CameraControlView
            }
        }
        imgPicker.delegate = self
        (imgPicker.cameraOverlayView as! CameraOverlayView).delegate = self
        return imgPicker
        }()
    lazy private var imagePickerControllerFromPhotoLibrary:UIImagePickerController = {
        let imgPicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            imgPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            if UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)?.isEmpty == false{
                imgPicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)!
                SZLOG(imgPicker.mediaTypes)
            }
        }
        imgPicker.delegate = self
        return imgPicker
        }()
    lazy private var imagePickerControllerFromExistedScenes:ExistedScenesPickerController = {
        let picker = ExistedScenesPickerController()
        picker.isPushFromSettings = false
        picker.delegate = self
        return picker
        }()
    private var pickedImageQualityFromCamera:CGFloat = 0.5 // 0 - 1
    
    //MARK: Other Properties
    var item:ItemMO?
    var scene:SceneMO?
    
    var longPressGestureRecognizer: UILongPressGestureRecognizer?
    var panGestureRecognizer: UIPanGestureRecognizer?
    var sceneTapGestureRecognizer: UITapGestureRecognizer?
    var itemTapGestureRecognizer: UITapGestureRecognizer?
    var isFirstTime = true
    var contentCategoryVC: CategoryPopupViewController?
    var contentDetailVC: DetailPopupViewController?
    var popupCategoryVC: FPPopoverController?
    var popupDetailVC: FPPopoverController?
    //MARK: Subview Properties
    lazy var naviTitleLable : UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = RecordStr
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(20)
        return naviTitleLable
        }()
    var naviCancel  = UIBarButtonItem()
    var naviSave    = UIBarButtonItem()
    var bgImage     = UIImageView()
    var sceneView   = UIImageView()
    var sceneLab    = UILabel()
    var sceneCmrBtn = UIButton()
    var itemImg     = UIImageView()
    var itemShadow  = UIImageView()
    var itemView    = UIImageView()
    var itemLab     = UILabel()
    var itemCmrBtn  = UIButton()
    var btns        = [UIButton](count: 2, repeatedValue: UIButton())
    var btnLabs     = [UILabel](count: 2, repeatedValue: UILabel())
    var btnImgViews = [UIImageView](count: 2, repeatedValue: UIImageView())
    var label       : UILabel?
    //MARK: Length Properties
    var tabBarH = CGFloat(65)
    var sceneY = CGFloat(64)
    var sceneW : CGFloat?
    var sceneH : CGFloat?
    var itemX  = CGFloat(15)
    var itemY  : CGFloat?
    var itemW  : CGFloat?
    var itemH  : CGFloat?
    var scaleD = CGFloat(180)//从 itemView 中点到 itemImg 缩到最小点之间的 y 轴距离
    var scaleY : CGFloat?
    var itemImgScale = CGFloat(0.3)
    var btnH   = CGFloat(60)
    var labelX : CGFloat?
    var dx: CGFloat?//拖动时 手指位置相对于图片左边缘的位置
    var ox: CGFloat?//sceneX 与 x=0 的距离
    var sceneImgWidthScaled: CGFloat?
    var pinScaleFrom = CGFloat(0.25)
    var pinScaleTo = CGFloat(0.45)
    
    //MARK: UIViewControllerDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        //(self.imagePickerControllerFromCamera.cameraOverlayView as! CameraOverlayView).delegate = self
        //长宽高计算
        sceneW = bgWidth
        sceneH = (bgHeight - sceneY - tabBarH)*0.6
        itemY = sceneY + sceneH! + itemX
        itemH = bgHeight - itemY! - tabBarH - itemX
        itemW = itemH
        scaleY = sceneY + sceneH! - scaleD
        labelX = itemX*2 + itemW!
        
        //背景
        bgImage = UIImageView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: bgHeight))
        bgImage.image = UIImage(named: BackgroundImageStr)
        
        //subviews
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPressGesture:")
        longPressGestureRecognizer?.numberOfTouchesRequired = 1
        longPressGestureRecognizer?.minimumPressDuration = 0.3
        longPressGestureRecognizer?.allowableMovement = 50
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: "handlePanGesture:")
        panGestureRecognizer?.maximumNumberOfTouches = 1
        panGestureRecognizer?.minimumNumberOfTouches = 1
        
        sceneTapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleSceneTapGesture")
        sceneTapGestureRecognizer?.numberOfTapsRequired = 1
        sceneTapGestureRecognizer?.numberOfTouchesRequired = 1
        itemTapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleItemTapGesture")
        itemTapGestureRecognizer?.numberOfTapsRequired = 1
        itemTapGestureRecognizer?.numberOfTouchesRequired = 1
        
        sceneView = UIImageView(frame: CGRect(x: 0, y: sceneY, width: sceneW!, height: sceneH!))
        //sceneView.backgroundColor = UIColor(white: 1, alpha: 0.3)
        sceneView.backgroundColor = UIColor(white: 0, alpha: 0.2)
        sceneView.addGestureRecognizer(sceneTapGestureRecognizer!)
        sceneView.addGestureRecognizer(panGestureRecognizer!)
        sceneLab = UILabel(frame: CGRect(x: 0, y: sceneH!/2 - 40 - 35, width: sceneW!, height: 30))
        sceneLab.text = sceneStr
        sceneLab.textAlignment = NSTextAlignment.Center
        sceneLab.textColor = UIColor.myBrownColor()
        sceneCmrBtn = UIButton(frame: CGRect(x: bgWidth/2 - 40, y: sceneY + sceneH!/2 - 40, width: 80, height: 80))
        sceneCmrBtn.setBackgroundImage(UIImage(named: "camera"), forState: UIControlState.Normal)
        sceneCmrBtn.addTarget(self, action: "sceneCameraTouched", forControlEvents: UIControlEvents.TouchDown)
        sceneView.addSubview(sceneLab)
        
        itemImg = UIImageView(frame: CGRect(x: itemX, y: itemY!, width: itemW!, height: itemH!))
        itemImg.addGestureRecognizer(longPressGestureRecognizer!)
        itemImg.addGestureRecognizer(itemTapGestureRecognizer!)
        itemShadow = UIImageView(frame: CGRect(x: itemX, y: itemY!, width: itemW!, height: itemH!))
        itemView = UIImageView(frame: CGRect(x: itemX, y: itemY!, width: itemW!, height: itemH!))
        //itemView.backgroundColor = UIColor(white: 1, alpha: 0.3)
        itemView.backgroundColor = UIColor(white: 0, alpha: 0.2)
        itemLab = UILabel(frame: CGRect(x: 0, y: 10, width: itemW!, height: 30))
        itemLab.text = itemStr
        itemLab.textAlignment = NSTextAlignment.Center
        itemLab.textColor = UIColor.myBrownColor()
        itemCmrBtn = UIButton(frame: CGRect(x: itemX + itemW!/2 - 40, y: itemY! + itemH!/2 - 40, width: 80, height: 80))
        itemCmrBtn.setBackgroundImage(UIImage(named: "camera"), forState: UIControlState.Normal)
        itemCmrBtn.addTarget(self, action: "itemCameraTouched", forControlEvents: UIControlEvents.TouchDown)
        itemView.addSubview(itemLab)
        
        var imgW = CGFloat(27)
        var btnsW = bgWidth/2
        for i in 0...1 {
            btns[i] = UIButton(frame: CGRect(x: btnsW*CGFloat(i), y: bgHeight - btnH, width: btnsW, height: btnH))
            btns[i].backgroundColor = UIColor(white: 1, alpha: 0.7)
            btnImgViews[i] = UIImageView(frame: CGRect(x: (btnsW - imgW)/2, y: 5, width: imgW, height: imgW))
            btnLabs[i] = UILabel(frame: CGRect(x: 0, y: 30, width: btnsW, height: 25))
            btnLabs[i].textAlignment = NSTextAlignment.Center
            btnLabs[i].textColor = UIColor.lightGrayColor()
            btnLabs[i].font = UIFont.systemFontOfSize(14)
            btns[i].addSubview(btnLabs[i])
            btns[i].addSubview(btnImgViews[i])
            self.view.addSubview(btns[i])
        }
        btns[0].addTarget(self, action: "btnCategoryTouched", forControlEvents: UIControlEvents.TouchUpInside)
        btns[1].addTarget(self, action: "btnDetailTouched", forControlEvents: UIControlEvents.TouchUpInside)
        btnLabs[0].text = categoryStr
        btnLabs[1].text = detailStr
        btnImgViews[0].image = UIImage(named: "Category")
        btnImgViews[1].image = UIImage(named: "Detail")
        
        label = UILabel(frame: CGRect(x: labelX!, y: itemY!, width: bgWidth - labelX!, height: itemH!))
        label!.numberOfLines = 0
        label!.myParagraphSpacedText(string: categoryStr + ":\n" + detailStr + ":\n")

        self.view.addSubview(bgImage)
        self.view.addSubview(naviTitleLable)
        self.view.addSubview(sceneView)
        self.view.addSubview(sceneCmrBtn)
        self.view.addSubview(itemView)
        self.view.addSubview(itemImg)
        self.view.addSubview(itemShadow)
        self.view.addSubview(itemCmrBtn)
        self.view.addSubview(label!)
        self.view.sendSubviewToBack(bgImage)
        
        //Navigation Bar
        naviCancel = UIBarButtonItem(title: cancelStr, style: UIBarButtonItemStyle.Plain, target: self, action: "btnCancelTouched")
        self.navigationItem.leftBarButtonItem = naviCancel
        naviSave = UIBarButtonItem(title: saveStr, style: UIBarButtonItemStyle.Plain, target: self, action: "btnSaveTouched")
        self.navigationItem.rightBarButtonItem = naviSave
        
        //Update
        if (item != nil){
            updateSceneImage(imageData: scene!.data)
            updateItemImage(imageData: item!.data)
            itemImg.image = UIImage(named: "pin")
            //ox = sceneView.center.x - sceneImgWidthScaled!/2
            ox = sceneView.center.x - sceneImgWidthScaled!/2
            itemImg.center.x = CGFloat(item!.positionX) + ox!//positionX 是相对图片左边缘的距离
            itemImg.center.y = CGFloat(item!.positionY)
            
//            var XFromPinToCenter = itemImg.center.x - sceneW!/2
//            itemImg.center.x = sceneW!/2
//            sceneView.center.x -= XFromPinToCenter
            //sceneView.center.x = max(min(0, location.x + dx!), bgWidth - sceneImgWidthScaled!) + sceneImgWidthScaled!/2

            isFirstTime = false
            UIView.animateWithDuration(0, animations: { () -> Void in
                self.itemImg.transform = CGAffineTransformMakeScale(self.pinScaleFrom, self.pinScaleFrom)
                }, completion: { (_) -> Void in
                    self.pinSwingAnimation()
            })
            label!.myParagraphSpacedText(string: categoryStr + ": " + NSLocalizedString(item!.relatedCategory!.name, comment: "" ) + "\n" + detailStr + ": \(item!.detail ?? emptyStr)\n")
        }

    }
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//        notificationCenter.addObserver(self, selector: "updateImageQuality", name: "imageQualityChanged", object: nil)
//    }
////    func updateImageQuality(){
////        self.imageQualitySliderValueChanged
////    }
//    override func viewWillDisappear(animated: Bool) {
//        super.viewWillDisappear(animated)
//    }


    //MARK: Init and Deinit
    required init(coder aDecoder: NSCoder) {
        //Do any init setup here
        super.init(coder: aDecoder)
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Gesture Handler
    func handleLongPressGesture(recognizer:UILongPressGestureRecognizer){
        if self.scene != nil{
            
            var location = CGPoint()
            location = CGPointMake(recognizer.locationInView(recognizer.view!.superview).x - 25, recognizer.locationInView(recognizer.view!.superview).y - 15)
            recognizer.view!.center = location
            itemShadow.center = recognizer.view!.center
            
            itemImg.layer.anchorPoint = CGPointMake(0.5, 0.5)
            
            //开始按下
            if (recognizer.state == UIGestureRecognizerState.Began){
                itemShadow.image = UIImage(named: "shadow")
                itemShadow.layer.shadowColor = UIColor.myBrownColor().CGColor
                itemShadow.layer.shadowOffset = CGSizeMake(5, 5)
                itemShadow.layer.shadowOpacity = 1
                
                if isFirstTime{//item 缩小 变圆
                    itemImgScale = 0.5 - (0.5 - 0.25)*(itemView.center.y - location.y)/scaleD
                    itemShadow.hidden = false
                    
                    UIView.animateWithDuration(0.2, animations: { () -> Void in
                        self.itemShadow.transform = CGAffineTransformMakeScale(self.itemImgScale, self.itemImgScale)
                        self.itemImg.transform = CGAffineTransformMakeScale(self.itemImgScale, self.itemImgScale)
                        self.itemImg.layer.masksToBounds = true
                        self.itemImg.layer.cornerRadius = self.itemH!/2
                        self.itemView.alpha = 0.4
                    })
                }else{//pin 放大
                    itemShadow.hidden = true
                    UIView.animateWithDuration(0.1, animations: { () -> Void in
                        self.itemImgScale = self.pinScaleTo
                        self.itemImg.transform = CGAffineTransformMakeScale(self.itemImgScale, self.itemImgScale)
                    })
                }
            }
                
                //拖动进行中
            else if ( recognizer.state != UIGestureRecognizerState.Ended && recognizer.state != UIGestureRecognizerState.Failed ) {
                //控制位置
                recognizer.view!.center = CGPointMake(location.x, max(location.y, sceneY - 10))
                itemShadow.center = recognizer.view!.center
                
                //控制大小
                if (itemImgScale <= pinScaleTo){
                    itemImgScale = pinScaleTo
                    itemImg.image = UIImage(named: "pin")
                    itemShadow.hidden = true
                }else{
                    itemImgScale = 0.5 - (0.5 - pinScaleTo)*(itemView.center.y - location.y)/scaleD
                    if (itemImgScale > 0.5){
                        itemImgScale = 0.5
                    }
                }
                if location.y > (sceneY + sceneH!) {
                    itemImgScale = 0.5 - (0.5 - pinScaleTo)*(itemView.center.y - location.y)/scaleD
                    if (itemImgScale > 0.5){
                        itemImgScale = 0.5
                    }
                    self.itemShadow.hidden = false
                    self.itemImg.image = UIImage(data: self.item!.data)
                    self.itemImg.layer.masksToBounds = true
                    self.itemImg.layer.cornerRadius = self.itemH!/2
                    UIView.animateWithDuration(0, animations: { () -> Void in
                        self.itemView.alpha = 0.4
                    })
                }
                
                UIView.animateWithDuration(0, animations: { () -> Void in
                    self.itemShadow.transform = CGAffineTransformMakeScale(self.itemImgScale, self.itemImgScale)
                    self.itemImg.transform = CGAffineTransformMakeScale(self.itemImgScale, self.itemImgScale)
                })
            }
                
                //长按手指放开时：
            else if (recognizer.state == UIGestureRecognizerState.Ended) {
                if (recognizer.locationInView(recognizer.view!.superview).y > sceneY)&&(recognizer.locationInView(recognizer.view!.superview).y < (sceneY + sceneH!)){//在scene里
                    self.itemImg.image = UIImage(named: "pin")
                    self.itemImg.center.y = max(location.y, self.sceneY)
                    self.itemShadow.hidden = true
                    self.itemView.alpha = 1
                    itemTapGestureRecognizer?.enabled = false
                    
                    if self.isFirstTime {
                        UIView.animateWithDuration(0.4, animations: { () -> Void in
                            self.itemImg.transform = CGAffineTransformMakeScale(self.pinScaleFrom, self.pinScaleFrom)
                            if recognizer.locationInView(recognizer.view!.superview).y > (self.sceneY + self.sceneH! - 20) {
                                recognizer.view!.center.x = recognizer.locationInView(recognizer.view!.superview).x
                                recognizer.view!.center.y = self.sceneY + self.sceneH! - recognizer.view!.frame.height/2
                            }else{
                                recognizer.view!.center = recognizer.locationInView(recognizer.view!.superview)
                            }
                            }, completion: { (_) -> Void in
                                //出现弹窗选择分类
                                self.categoryPopupShow()
                        })
                        self.isFirstTime = false
                    }else{//pin 缩小
                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                            self.itemImg.transform = CGAffineTransformMakeScale(self.pinScaleFrom, self.pinScaleFrom)
                            if recognizer.locationInView(recognizer.view!.superview).y > (self.sceneY + self.sceneH! - 20) {
                                recognizer.view!.center.x = recognizer.locationInView(recognizer.view!.superview).x
                                recognizer.view!.center.y = self.sceneY + self.sceneH! - recognizer.view!.frame.height/2
                            }else{
                                recognizer.view!.center = recognizer.locationInView(recognizer.view!.superview)
                            }
                            }, completion: { (_) -> Void in
                                //小指针晃动
                                self.pinSwingAnimation()
                        })
                    }
                    //保存
                    positionX = Double(recognizer.view!.center.x - (sceneView.center.x - sceneImgWidthScaled!/2))//相对图片左边缘值
                    SZLOG(recognizer.view!.center.x - sceneView.center.x + sceneImgWidthScaled!/2)
                    positionY = Double(recognizer.view!.center.y)
                    if self.item != nil && self.scene != nil {
                        ItemMO.placeItemOnScene(item: self.item!, scene: self.scene!, positionXInScene: positionX, positionYInScene: positionY, itemWidthInScene: 0, itemHeightInScene: 0, detail: self.item?.detail, category: item?.relatedCategory?.name)
                    }
                }
                else{//scene外
                    UIView.animateWithDuration(0.2, animations: { () -> Void in
                        self.itemShadow.hidden = true
                        self.itemShadow.transform = CGAffineTransformMakeScale(1, 1)
                        self.itemShadow.center = self.itemView.center
                        self.itemImg.image = UIImage(data: self.item!.data)
                        self.itemImg.transform = CGAffineTransformMakeScale(1, 1)
                        self.itemImg.center = self.itemView.center
                        self.itemImg.layer.cornerRadius = 0
                        self.itemView.alpha = 1
                    })
                    isFirstTime = true
                    itemTapGestureRecognizer?.enabled = true
                    //删除
                    ItemMO.cancelPlaceItemOnScene(item: self.item!)
                    SZLOG("self.item!.detail=\(self.item!.detail)")
                    label?.myParagraphSpacedText(string: categoryStr + ": \n" + detailStr + ": \n")
                }
            }
        }
    }
    
    func pinSwingAnimation(){
        var rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation.duration = 0.1
        rotation.fromValue = -M_PI/6
        rotation.toValue = M_PI/6
        rotation.autoreverses = true
        rotation.repeatCount = 3
        itemImg.layer.anchorPoint = CGPointMake(0.5, 1)
        itemImg.layer.addAnimation(rotation, forKey: "rotation")
    }
    
    func handlePanGesture(recognizer:UIPanGestureRecognizer){
        if self.scene != nil{
            if sceneView.image?.size.width > sceneView.image?.size.height {

                var location = recognizer.locationInView(recognizer.view!.superview)
                if recognizer.state == UIGestureRecognizerState.Began{
                    dx = recognizer.view!.center.x - sceneImgWidthScaled!/2 - location.x
                }else{
                    recognizer.view!.center.x = max(min(0, location.x + dx!), bgWidth - sceneImgWidthScaled!) + sceneImgWidthScaled!/2
                    if itemImg.transform.a == pinScaleFrom {
                        itemImg.center.x = recognizer.view!.center.x - sceneImgWidthScaled!/2 + CGFloat(item!.positionX)
                    }
                }
            }
        }
    }
    
    func handleSceneTapGesture(){
        sceneCameraTouched()
    }
    
    func handleItemTapGesture(){
        itemCameraTouched()
    }
    
    
    //MARK: PopoverWindow
    func popoverControllerDidDismissPopover(popoverController: FPPopoverController!) {
        if popoverController == popupCategoryVC{
            btnLabs[0].textColor = UIColor.lightGrayColor()
            btnImgViews[0].image = UIImage(named: "Category")
        }else if popoverController == popupDetailVC{
            contentDetailVC?.textView.resignFirstResponder()
            btnLabs[1].textColor = UIColor.lightGrayColor()
            btnImgViews[1].image = UIImage(named: "Detail")
        }
    }

    func parentPopupDismiss(popupWin: PopupWinType) {
        if popupWin == PopupWinType.CategoryPopupWin {
            popupCategoryVC?.dismissPopoverAnimated(true)
        }else if popupWin == PopupWinType.DetailPopupWin{
            popupDetailVC?.dismissPopoverAnimated(true)
        }
    }
    
    func saveCategoryAndDetail(popupWin: PopupWinType) {
        if popupWin == PopupWinType.CategoryPopupWin {
            if contentCategoryVC?.category != nil{
                self.item!.relatedCategory = contentCategoryVC!.category
            }
        }else if popupWin == PopupWinType.DetailPopupWin{
            if contentDetailVC?.detail != nil{
                self.item?.detail = contentDetailVC?.detail
                SZLOG("self.item?.detail=\(self.item?.detail)")
            }
        }
        label?.myParagraphSpacedText(string: categoryStr + ": " + NSLocalizedString(item!.relatedCategory!.name, comment: "" ) + "\n" + detailStr + ": \(item!.detail ?? emptyStr)\n")
    }
    
    func categoryPopupShow(){
        btnLabs[0].textColor = UIColor.myBrownColor()
        btnLabs[1].textColor = UIColor.lightGrayColor()
        btnImgViews[0].image = UIImage(named: "CategorySelected")
        btnImgViews[1].image = UIImage(named: "Detail")
        
        contentCategoryVC = CategoryPopupViewController()
        contentCategoryVC!.category = item!.relatedCategory
        contentCategoryVC!.delegate = self
        var popupY = bgHeight*100.0/667.0
        var popupW = contentCategoryVC!.popupW
        var popupH = contentCategoryVC!.popupH
        var popupX = (bgWidth - popupW)/2
        popupCategoryVC = FPPopoverController(viewController: contentCategoryVC)
        popupCategoryVC!.contentSize = CGSizeMake(popupW, popupH)
        popupCategoryVC!.arrowDirection = FPPopoverNoArrow
        popupCategoryVC!.delegate = self
        popupCategoryVC!.presentPopoverFromPoint(CGPoint(x: popupX, y: popupY))
    }
    
    func detailPopupShow(){
        btnLabs[0].textColor = UIColor.lightGrayColor()
        btnLabs[1].textColor = UIColor.myBrownColor()
        btnImgViews[0].image = UIImage(named: "Category")
        btnImgViews[1].image = UIImage(named: "DetailSelected")
        
        contentDetailVC = DetailPopupViewController()
        SZLOG("self.item?.detail=\(self.item?.detail)")
        contentDetailVC!.detail = self.item!.detail
        contentDetailVC!.delegate = self
        var popupY = bgHeight*100.0/667.0
        var popupW = contentDetailVC!.popupW
        var popupH = contentDetailVC!.popupH
        var popupX = (bgWidth - popupW)/2
        popupDetailVC = FPPopoverController(viewController: contentDetailVC)
        popupDetailVC!.contentSize = CGSizeMake(popupW, popupH)
        popupDetailVC!.arrowDirection = FPPopoverNoArrow
        popupDetailVC!.delegate = self
        popupDetailVC!.presentPopoverFromPoint(CGPoint(x: popupX, y: popupY))
    }
    
    
    //MARK: Scene and Item button
    func sceneCameraTouched(){
        self.typeOfAcquiredPhoto = PhotoType.Scene
        self.showPhotoAcquiringActionSheet()
    }
    
    func itemCameraTouched(){
        self.typeOfAcquiredPhoto = PhotoType.Item
        self.showPhotoAcquiringActionSheet()
    }
    
    func updateSceneImage(imageData data:NSData){
        sceneView.image = UIImage(data: data)
        //初始化图片位置，放在正中间
        //SZLOG(self.item?.positionX)
//        if self.item?.positionX > 0 && self.item?.positionX > 0{
//            sceneView.center.x = 700//sceneW!/2 + CGFloat(self.item!.positionX)
//        }else{
            sceneView.center.x = sceneW!/2
//        }
        //sceneView.center.x = sceneW!/2
        //根据图片实际大小改变sceneView大小
        sceneImgWidthScaled = sceneH!/sceneView.image!.size.height*sceneView.image!.size.width
        var scaleX = sceneImgWidthScaled!/sceneW!
//        if self.item?.positionX > 0 && self.item?.positionX > 0{
//            var XFromPinToCenter = itemImg.center.x - sceneW!/2
//            itemImg.center.x = sceneW!/2
//            sceneView.center.x -= XFromPinToCenter
//        }else{
//            sceneView.center.x = sceneW!/2
//        }
        
        sceneView.transform = CGAffineTransformMakeScale(scaleX, 1)

        sceneView.userInteractionEnabled = true
        sceneCmrBtn.hidden = true
        sceneCmrBtn.transform = CGAffineTransformMakeScale(2, 2)
        sceneCmrBtn.frame.origin.x = 0
        sceneCmrBtn.frame.origin.y = self.sceneY
        sceneLab.hidden = true
        
        //每次修改场景照片，item位置清零
        if isFirstTime == false{
            itemImg.layer.anchorPoint = CGPointMake(0.5, 0.5)
            itemShadow.hidden = true
            itemShadow.transform = CGAffineTransformMakeScale(1, 1)
            itemShadow.center = self.itemView.center
            itemImg.image = UIImage(data: self.item!.data)
            itemImg.transform = CGAffineTransformMakeScale(1, 1)
            itemImg.center = self.itemView.center
            itemImg.layer.cornerRadius = 0
            itemView.alpha = 1
            itemTapGestureRecognizer?.enabled = true
            //删除
            ItemMO.cancelPlaceItemOnScene(item: self.item!)
            SZLOG("self.item!.detail=\(self.item!.detail)")
            label?.myParagraphSpacedText(string: categoryStr + ": \n" + detailStr + ": \n")
        }
    }
    
    func updateItemImage(imageData data:NSData){
        itemImg.image = UIImage(data: data)
        itemView.image = UIImage(data: data)
        itemImg.userInteractionEnabled = true
        itemCmrBtn.hidden = true
        UIView.animateWithDuration(0, animations: { () -> Void in
            self.itemCmrBtn.transform = CGAffineTransformMakeScale(2, 2)
            self.itemCmrBtn.frame.origin.x = self.itemX
            self.itemCmrBtn.frame.origin.y = self.itemY!
        })
        itemLab.hidden = true
        self.view.bringSubviewToFront(itemImg)
    }
    
    func btnCategoryTouched(){
        if item != nil {
            self.categoryPopupShow()
        }
    }
    
    func btnDetailTouched(){
        if item != nil {
            self.detailPopupShow()
        }
    }
    
    
    //MARK: NavigationBar Button
    func btnCancelTouched(){
        var transition = CATransition()
        transition.duration = 0.2
        transition.subtype = kCATransitionFromBottom
        self.navigationController?.view.layer.addAnimation(transition, forKey: nil)
        appDelegate.managedObjectContext!.rollback()
        self.navigationController?.popToRootViewControllerAnimated(false)
    }
    
    func btnSaveTouched(){
        var transition = CATransition()
        transition.duration = 0.2
        transition.subtype = kCATransitionFromBottom
        self.navigationController?.view.layer.addAnimation(transition, forKey: nil)
        if self.item?.containerScene != nil {
            appDelegate.saveContext()
            self.navigationController?.popToRootViewControllerAnimated(false)
        }else{
            SZLOG("save failed, not enough data iuput")
            UIAlertView(title: alertViewTitle, message: alertViewMsg, delegate: nil, cancelButtonTitle: popupOkBtnStr).show()
        }
    }

    
    //MARK: AlertController, AlertAction and related delegate
    func showPhotoAcquiringActionSheet(){
        if typeOfAcquiredPhoto == PhotoType.Scene{
            self.photoAcquiringActionSheetForScene.showInView(self.view)
        }else{
            self.photoAcquiringActionSheetForItem.showInView(self.view)
        }
        
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int){
        SZLOG("button index = \(buttonIndex)")
        switch buttonIndex{
        case 0: SZLOG("Cancel Button Click")
        case 2: self.showCamera()
        case 1: self.showPhotoLibrary()
        case 3: self.showExistedScenesCollectionView()
        SZLOG("Choose existed scene in local disk, usually an already used scene by other item")
        default: SZLOG("Unknown Error")
        }
        SZLOG("Did click button at action sheet")
    }
    
    
    //MARK: PhotosPicking and related delegate
    func showPhotoLibrary(){
        presentViewController(self.imagePickerControllerFromPhotoLibrary, animated: true, completion: { () in
            SZLOG("Did show photo library")
        })
    }
    
    func showCamera(){
        presentViewController(self.imagePickerControllerFromCamera, animated: true, completion: { () in
            SZLOG("Did show Camera")
        })
    }
    
    func showExistedScenesCollectionView(){
        self.navigationController!.pushViewController(self.imagePickerControllerFromExistedScenes, animated: true)
    }
    
    func imageQualitySliderValueChanged(sliderValue value:Float){
        self.pickedImageQualityFromCamera = CGFloat(value)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]){
        SZLOG("Did finish picking media with info")
        var imageData: NSData?
        let image:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let now = NSDate()
        switch self.typeOfAcquiredPhoto{
        case PhotoType.Item:
            SZLOG("type of photo is item")  //Establish New Item
//            var sourceCGImg = image.CGImage
//            var rect:CGRect?
//            rect = CGRectMake(abs(image.size.height - image.size.width) / 2, 0, min(image.size.height,image.size.width), min(image.size.height,image.size.width))
//            var subImg = UIImage(CGImage: CGImageCreateWithImageInRect(sourceCGImg, rect!))
//            subImg = UIImage(CGImage: subImg!.CGImage, scale: 1, orientation: image.imageOrientation)
//            imageData = UIImageJPEGRepresentation(subImg,self.pickedImageQualityFromCamera)
            
            
            imageData = UIImageJPEGRepresentation(image.croppedImage(imageSizeWidth: 640, imageSizeHeight: 640),self.pickedImageQualityFromCamera)
            
            if self.item == nil{
                self.item = ItemMO.insertNewItem(creationDate: now, localIdentifier: now.description, imageData: imageData!)
            }else{
                self.item!.data = imageData!
            }
            self.updateItemImage(imageData: self.item!.data)
        case PhotoType.Scene:
            SZLOG("type of photo is scene") //Establish New Scene
            
            //imageData = UIImageJPEGRepresentation(image.croppedImage(imageSizeWidth: sceneW!, imageSizeHeight: sceneW!/image.size.width*image.size.height), self.pickedImageQualityFromCamera)
            imageData = UIImageJPEGRepresentation(image, self.pickedImageQualityFromCamera)
            
//            if self.scene == nil{
                self.scene = SceneMO.insertNewScene(creationDate: now, localIdentifier: now.description, imageData: imageData!)
//                //self.scene!.data = imageData!
//            }else{
//                self.scene!.data = imageData!
//            }
            self.updateSceneImage(imageData: self.scene!.data)
        default:
            SZLOG("Error: wrong type of photo")
        }
        dismissViewControllerAnimated(true, completion: { () in
            SZLOG("Did dismiss ImagePicker controller")
        })
        
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        SZLOG("Did cancel picking media")
        dismissViewControllerAnimated(true, completion: { () in
            SZLOG("Did dismiss ImagePicker controller")
        })
    }
    
    func existedScenesPickerController(existedScenesPicker picker:ExistedScenesPickerController,didFinishPickingScene scene:SceneMO){
        SZLOG("did finish picking scene")
        self.scene = scene
        self.updateSceneImage(imageData: scene.data)
    }
    
    func existedScenesPickerControllerDidCancel(existedScenesPicker picker:ExistedScenesPickerController){
        SZLOG("did cancel picking scene")
        
    }
}

