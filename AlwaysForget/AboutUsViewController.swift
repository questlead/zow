//
//  AboutUsViewController().swift
//  AlwaysForget
//
//  Created by User on 14/08/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
private let ContactUsStr:String = NSLocalizedString("Contact us",comment: "Contact us string in about us vc")
private let TermOfServiceStr:String = NSLocalizedString("Term Of Service",comment: "Term of service string in about us vc")
class AboutUsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    //MARK:Properties
    lazy var naviBarView:UIView = {
        let naviBarView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: 64))
        naviBarView.backgroundColor = UIColor.myBlueColor()
        return naviBarView
        }()
    lazy private var aboutUsTableView:UITableView = {
        let table = UITableView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: bgHeight), style: UITableViewStyle.Grouped)
        table.delegate = self
        table.dataSource = self
        let labelY = CGFloat(64.0 + 100.0)
        SZLOG(table.rowHeight)
        let contactUslabel = UILabel(frame: CGRect(x: table.frame.origin.x, y: labelY, width: table.frame.width, height: table.frame.height - labelY))
        //contactUslabel.backgroundColor = UIColor.blueColor()
        //contactUslabel.text = "邮箱：silver.wr.sr.7@gmail.com"
        table.addSubview(contactUslabel)
        return table
        }()
//    lazy private var contactUsLabel:UILabel = {
//        let label = UILabel(frame: CGRect(x: <#CGFloat#>, y: <#CGFloat#>, width: <#CGFloat#>, height: <#CGFloat#>))
//        }()
    //MARK: TableView Delegate and datasource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch section{
        case 0:
            return 1
        case 1:
            return 0
        default:
            SZLOG("unknown error")
            return 1
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "aboutUsTableCell")
        var cell = tableView.dequeueReusableCellWithIdentifier("aboutUsTableCell", forIndexPath: indexPath) as! UITableViewCell
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = TermOfServiceStr
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        case 1:
            cell.textLabel?.text = ContactUsStr
            cell.selectionStyle = UITableViewCellSelectionStyle.None
        default:
            SZLOG("unkonwn error")
        }
        cell.setEditing(false, animated: false)
        //cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return AboutUsStr
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section != 0{
            return
        }
        switch indexPath.row{
        case 0:
            self.navigationController?.pushViewController(TermOfServiceViewController(), animated: true)
//        case 1:
//            self.navigationController?.pushViewController(TermOfServiceViewController(), animated: true)
            
        default:
            SZLOG("unknown error")
        }
        tableView.cellForRowAtIndexPath(indexPath)?.selected = false
        
    }
    //MARK:UIViewControllerDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.aboutUsTableView)
        self.view.addSubview(naviBarView)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
