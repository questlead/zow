//
//  CameraOverlayView.swift
//  AlwaysForget
//
//  Created by User on 9/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

private let imageQualitySliderTitleStr:String = NSLocalizedString("Image Quality",comment: "image Quality Slider Title Str")
private let ImageQualityLowIndicatorStr = NSLocalizedString("Low",comment: "Indicating the image quality when catching a image from camera")
private let ImageQualityHighIndicatorStr = NSLocalizedString("High",comment: "Indicating the image quality when catching a image from camera")
private let ImageQualityHigh:Float = 0.7
private let ImageQualityMedium:Float = 0.5
private let ImageQualityLow:Float = 0.3

@objc protocol ImageQualitySliderDelegate {
    optional func imageQualitySliderValueChanged(sliderValue value:Float)
}
class CameraOverlayView: UIView {
    //MARK: prorerties
    lazy private var imageQualitySlider:UISlider = {
        let slider = UISlider(frame: CGRect(x: 50, y: 15, width: bgWidth - 50 - 50, height: 20))
        //        slider.minimumValueImage = UIImage(named: "")
        //        slider.maximumValueImage = UIImage(named: "")
                slider.value = 0.8
        //        slider.backgroundColor = UIColor.blueColor()
        //slider.maximumValue = ImageQualityHigh
        //slider.minimumValue = ImageQualityLow
        return slider
        }()
    lazy private var lowQualityLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 15, width: 50, height: 20))
        label.text = ImageQualityLowIndicatorStr
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.whiteColor()
        //        label.backgroundColor = UIColor.magentaColor()
        //        label.alpha = 0.5
        return label
        }()
    lazy private var highQualityLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: bgWidth - 50, y: 15, width: 50, height: 20))
        label.text = ImageQualityHighIndicatorStr
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        //        label.backgroundColor = UIColor.magentaColor()
        //        label.alpha = 0.5
        return label
        }()
    lazy private var sliderTitleLabel:UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 25, width: bgWidth, height: 20))
        label.text = imageQualitySliderTitleStr
        label.font = UIFont.systemFontOfSize(10)
        label.textColor = UIColor.grayColor()
        label.textAlignment = NSTextAlignment.Center
        //        label.backgroundColor = UIColor.magentaColor()
        //        label.alpha = 0.5
        label.userInteractionEnabled = false
        return label
        }()
    //    lazy private var takingPictureButton:UIButton = {
    //        let btn = UIButton(frame: CGRect(x: bgWidth / 2 - 25, y: 40, width: 50, height: 40))
    //        btn.setTitle("TakingPic", forState: UIControlState.Normal)
    //        btn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    //        btn.addTarget(self, action: "didTakePicture", forControlEvents: UIControlEvents.TouchDragInside)
    //        return btn
    //        }()
    weak var delegate:ImageQualitySliderDelegate?
    
    //MARK: init and deinit
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.blackColor()
        self.alpha = 0.9
        
        self.imageQualitySlider.addTarget(self, action: "imageQualityValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        self.addSubview(self.sliderTitleLabel)
        self.addSubview(self.imageQualitySlider)
        self.addSubview(self.lowQualityLabel)
        self.addSubview(self.highQualityLabel)
        
        //self.addSubview(self.takingPictureButton)
    }
    func imageQualityValueChanged(slider:UISlider){
        //SZLOG("value did changed = \(slider.value)")
        var imageQuality:Float = ImageQualityHigh
        if slider.value >= 0 && slider.value < 0.3 {
            imageQuality = ImageQualityLow
            //self.imageQualitySlider.setValue(0, animated: true)
        } else if slider.value >= 0.3 && slider.value < 0.7 {
            imageQuality = ImageQualityMedium
            //self.imageQualitySlider.setValue(0.5, animated: true)
        } else if slider.value >= 0.7 && slider.value <= 1 {
            imageQuality = ImageQualityHigh
            //self.imageQualitySlider.setValue(1, animated: true)
        } else {
            SZLOG("unknown error")
        }
        self.delegate?.imageQualitySliderValueChanged?(sliderValue: imageQuality)
        var notification = NSNotification(name: "imageQualityChanged", object: imageQuality)
        notificationCenter.postNotification(notification)
    }
    //btn action 
    //    func didTakePicture(){
    //    }
}
