//
//  CategoryPopupViewController.swift
//  AlwaysForget
//
//  Created by User on 21/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

private let popupWinTitleText = NSLocalizedString("Category",comment: "Choose Category")


class CategoryPopupViewController: PopupViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    //MARK: Properties
    var flowLayOut = UICollectionViewFlowLayout()
    var collectionView: UICollectionView?
    var category: CategoryMO?
    weak var delegate: FPPopoverControllerDelegate?
    
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var collectionViewWidth = popupW - 10
        var cellW = collectionViewWidth/4
        
        popupWinTitle.text = popupWinTitleText
        
        flowLayOut.itemSize = CGSizeMake(cellW, cellW)
        flowLayOut.minimumInteritemSpacing = 0
        flowLayOut.minimumLineSpacing = 0
        flowLayOut.scrollDirection    = UICollectionViewScrollDirection.Vertical
        collectionView = UICollectionView(frame: CGRect(x: 5, y: titleH, width: collectionViewWidth, height: popupH - titleH - btnH), collectionViewLayout: flowLayOut)
        collectionView?.backgroundColor = UIColor.whiteColor()
        collectionView?.dataSource = self
        collectionView?.delegate   = self
        
        self.view.addSubview(collectionView!)
    }
    
    //MARK: CollectionView Func
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appDelegate.categories.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerClass(CollectionViewCategoryCell.self, forCellWithReuseIdentifier: "CategoryCell")
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryCell", forIndexPath: indexPath) as! CollectionViewCategoryCell
        cell.category.text = NSLocalizedString(appDelegate.categories[indexPath.item].name,comment: "name of category")
        cell.category.textColor = UIColor.lightGrayColor()
        if let availableImage = UIImage(named: appDelegate.categories[indexPath.row].name){
            cell.categoryImg.image = availableImage
        }else{
            cell.categoryImg.image = UIImage(named: "userCategory")
        }
        if category?.name == appDelegate.categories[indexPath.item].name {
            cell.category.textColor = UIColor.myBrownColor()
            if let availableImage = UIImage(named: "\(category!.name)Selected"){
                cell.categoryImg.image = availableImage
            }else{
                cell.categoryImg.image = UIImage(named: "userCategorySelected")
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        category = CategoryMO.fetchCategory(categoryName: appDelegate.categories[indexPath.item].name)
        collectionView.reloadData()
    }
    
    override func btnNextTouched() {
        self.delegate?.parentPopupDismiss!(PopupWinType.CategoryPopupWin)
        self.delegate?.saveCategoryAndDetail!(PopupWinType.CategoryPopupWin)
        self.delegate?.btnDetailTouched!()
        
    }
    
    override func btnOkTouched() {
        self.delegate?.parentPopupDismiss!(PopupWinType.CategoryPopupWin)
        self.delegate?.saveCategoryAndDetail!(PopupWinType.CategoryPopupWin)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
