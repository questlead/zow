//
//  CollectionViewCategoryCell.swift
//  AlwaysForget
//
//  Created by User on 12/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

class CollectionViewCategoryCell: UICollectionViewCell {
    var categoryImg = UIImageView()
    var category = UILabel()
    weak var delegate:CategoryPickerControllerDelegate?

    var assoicatedCategoryMO: CategoryMO!
    
    var imgW = CGFloat(27)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //self.backgroundColor = UIColor.whiteColor()
        
        categoryImg = UIImageView(frame: CGRect(x: (frame.width - imgW)/2, y: 5, width: imgW, height: imgW))
        category = UILabel(frame: CGRect(x: 0, y: 30, width: frame.width, height: 25))
        category.textAlignment = NSTextAlignment.Center
        category.textColor = UIColor.myBrownColor()
        category.font = UIFont.systemFontOfSize(14)
        
        self.addSubview(categoryImg)
        self.addSubview(category)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
