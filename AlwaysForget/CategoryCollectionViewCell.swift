
//
//  CategoryCollectionViewCell.swift
//  AlwaysForget
//
//  Created by User on 5/08/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

let alertViewForDeletingCategoryStr:String = NSLocalizedString("Delete this category?",comment: "Alert View For Deleting Category: Title")
let alertViewMessageForDeletingCategoryStr:String = NSLocalizedString("Default category cannot be deleted.",comment: "Alert View For Deleting Category: Message")

class CategoryCollectionViewCell: UICollectionViewCell,UIAlertViewDelegate {
    var categoryImg = UIImageView()
    var category = UILabel()
    var deleteBtn: UIButton?
    var isDeleteBtnHidden = true{
        willSet{
            if newValue == false{
                self.delegate?.enableTapGesture!()
            }else{
                self.delegate?.disenableTapGesture!()
            }
        }
    }
    lazy var selectBtn: UIButton = {
        var selectBtn = UIButton(frame: CGRect(x: bgWidth/3 - 40, y: bgWidth/3 - 40, width: 40, height: 40))
        selectBtn.addTarget(self, action: "selectBtnTouched", forControlEvents: UIControlEvents.TouchUpInside)
        selectBtn.hidden = true
        selectBtn.userInteractionEnabled = false
        return selectBtn
        }()
    var isSelectBtnSelected = false{
        willSet{
            if newValue == false{
                selectBtn.setBackgroundImage(UIImage(named: "select2"), forState: UIControlState.Normal)
            }else{
                selectBtn.setBackgroundImage(UIImage(named: "selectSelected"), forState: UIControlState.Normal)
            }
        }
    }
    weak var delegate:CategoryPickerControllerDelegate?
    
    var longPressGestureRecognizer: UILongPressGestureRecognizer?
    var assoicatedCategoryMO: CategoryMO!
    
    lazy var alertViewForDeletingScene:UIAlertView? = {
        return UIAlertView(title: alertViewForDeletingCategoryStr, message: alertViewMessageForDeletingCategoryStr, delegate: self, cancelButtonTitle: cancelStr, otherButtonTitles: popupOkBtnStr)
        }()
    var imgW = CGFloat(36)
    var imgY = CGFloat(0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //self.backgroundColor = UIColor.whiteColor()
        self.layer.borderColor = UIColor.myOpaqueGray().CGColor
        self.layer.borderWidth = 0.5
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPressGesture:")
        longPressGestureRecognizer!.numberOfTouchesRequired = 1
        longPressGestureRecognizer!.allowableMovement = 50
        longPressGestureRecognizer!.minimumPressDuration = 0.5
        
        imgY = (frame.height - imgW - 30)/2
        categoryImg = UIImageView(frame: CGRect(x: (frame.width - imgW)/2, y: imgY, width: imgW, height: imgW))
        category = UILabel(frame: CGRect(x: 0, y: imgY + imgW, width: frame.width, height: 40))
        category.textAlignment = NSTextAlignment.Center
        category.textColor = UIColor.myBrownColor()
        category.font = UIFont.systemFontOfSize(18)
        
        deleteBtn = UIButton(frame: CGRect(x: frame.width - 40, y: 0, width: 40, height: 40))
        //deleteBtn?.backgroundColor = UIColor.redColor()
        //deleteBtn?.layer.cornerRadius = deleteBtn!.frame.height/2
        deleteBtn?.setBackgroundImage(UIImage(named: "delete"), forState: UIControlState.Normal)
        deleteBtn?.addTarget(self, action: "deleteBtnTouched", forControlEvents: UIControlEvents.TouchUpInside)
        deleteBtn?.hidden = true
        
        isDeleteBtnHidden = deleteBtn!.hidden

        //self.addGestureRecognizer(longPressGestureRecognizer!)
        self.addSubview(deleteBtn!)
        self.addSubview(categoryImg)
        self.addSubview(category)
        self.addSubview(selectBtn)

    }
    
    //MARK:Long Press Recognizer
    func handleLongPressGesture(recognizer:UILongPressGestureRecognizer){
        if (recognizer.state == UIGestureRecognizerState.Began){
            SZLOG("long press")
            deleteBtn?.hidden = false
            isDeleteBtnHidden = deleteBtn!.hidden
        }
    }
    //MARK: delete scenes and related alert view
    func deleteBtnTouched(){
        SZLOG(alertViewForDeletingScene)
        self.alertViewForDeletingScene!.show()
        
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        switch buttonIndex{
        case 0:
            SZLOG("cancel button click")
            deleteBtn?.hidden = true
            isDeleteBtnHidden = deleteBtn!.hidden
        case 1:
            SZLOG("confirm button click")
            deleteBtn?.hidden = true
            isDeleteBtnHidden = deleteBtn!.hidden
            CategoryMO.deleteCategory(category: assoicatedCategoryMO)
            appDelegate.saveContext()
        default:
            SZLOG("unkown error")
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
