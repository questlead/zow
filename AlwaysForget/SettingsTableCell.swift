//
//  SettingsTableCell.swift
//  AlwaysForget
//
//  Created by User on 26/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

class SettingsTableCell: UITableViewCell {

    var settingViewW: CGFloat?
    var imgView: UIImageView?
    var txtLabel: UILabel?
    var seperator1: UIView?
    var seperator2: UIView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        settingViewW = bgWidth/375*300
        self.backgroundColor = UIColor.myWhiteColor()
        txtLabel = UILabel(frame: CGRect(x: 45, y: 10, width: settingViewW! - 45, height: 25))
        txtLabel!.textColor = UIColor.myBrownColor()
        imgView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25))

        seperator1 = UIView(frame: CGRect(x: 50, y: 49, width: settingViewW! - 80, height: 1))
        seperator1?.backgroundColor = UIColor(white: 0.9, alpha: 1)
        //seperator1?.backgroundColor = UIColor.blackColor()
        seperator2 = UIView(frame: CGRect(x: 0, y: 0, width: settingViewW!, height: 1))
        //seperator2?.backgroundColor = UIColor.darkGrayColor()
        
        self.addSubview(imgView!)
        self.addSubview(txtLabel!)
        self.addSubview(seperator1!)
        self.addSubview(seperator2!)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
