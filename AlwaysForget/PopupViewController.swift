//
//  PopupViewController.swift
//  AlwaysForget
//
//  Created by User on 24/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

private let popupNextBtnStr = NSLocalizedString("Next",comment: "Button To Next Popup Window")
let popupOkBtnStr = NSLocalizedString("Confirm",comment: "OK Button")

class PopupViewController: UIViewController {
    //MARK: Properties
    var popupWinTitle = UILabel()
    var nextBtn  = UIButton()
    var okBtn    = UIButton()
    var lineHorrizontalUp = UIView()
    var lineHorrizontalDown = UIView()
    var lineVertical = UIView()
    //MARK: Height & Width
    var popupW = CGFloat(280)
    var popupH = CGFloat(300)
    var titleH   = CGFloat(50)
    var btnH     = CGFloat(50)

    override func viewDidLoad() {
        super.viewDidLoad()

        popupWinTitle = UILabel(frame: CGRect(x: 0, y: 0, width: popupW, height: titleH))
        popupWinTitle.textAlignment = NSTextAlignment.Center
        popupWinTitle.textColor     = UIColor.myBlueColor()
        popupWinTitle.font          = UIFont.systemFontOfSize(20)
        
        nextBtn = UIButton(frame: CGRect(x: popupW/2, y: popupH - btnH, width: popupW/2, height: btnH))
        nextBtn.setTitleColor(UIColor.myBrownColor(), forState: UIControlState.Normal)
        nextBtn.setTitle(popupNextBtnStr, forState: UIControlState.Normal)
        nextBtn.addTarget(self, action: "btnNextTouched", forControlEvents: UIControlEvents.TouchUpInside)
        
        okBtn = UIButton(frame: CGRect(x: 0, y: popupH - btnH, width: popupW/2, height: btnH))
        okBtn.setTitleColor(UIColor.myBrownColor(), forState: UIControlState.Normal)
        okBtn.setTitle(popupOkBtnStr, forState: UIControlState.Normal)
        okBtn.addTarget(self, action: "btnOkTouched", forControlEvents: UIControlEvents.TouchUpInside)
        
        lineHorrizontalUp = UIView(frame: CGRect(x: 5, y: titleH - 1, width: popupW - 10, height: 1))
        lineHorrizontalUp.backgroundColor = UIColor.myBlueColor()
        lineHorrizontalDown = UIView(frame: CGRect(x: 5, y: popupH - btnH, width: popupW - 10, height: 1))
        lineHorrizontalDown.backgroundColor = UIColor.lightGrayColor()
        
        lineVertical = UIView(frame: CGRect(x: popupW/2, y: popupH - btnH, width: 1, height: btnH))
        lineVertical.backgroundColor = UIColor.lightGrayColor()
        
        self.view.addSubview(popupWinTitle)
        self.view.addSubview(nextBtn)
        self.view.addSubview(okBtn)
        self.view.addSubview(lineHorrizontalUp)
        self.view.addSubview(lineHorrizontalDown)
        self.view.addSubview(lineVertical)
    }
    
    //MARK: Button function
    func btnNextTouched(){
        
    }
    
    func btnOkTouched(){
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
