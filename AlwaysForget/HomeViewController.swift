//
//  HomeViewController.swift
//  AlwaysForget
//
//  Created by User on 6/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//
// 2015.08.17 会议记录：
// feedback 发送成功：键盘，翻译
// search bar：键盘
// 备注：键盘
// 背景删一个

import UIKit

let RecordStr = NSLocalizedString("Record",comment: "New record")
private let DataManagementStr = NSLocalizedString("Management",comment: "managers ")//Data Management
private let MoreStr = NSLocalizedString("More",comment: "More comments")
let ScenesStr = NSLocalizedString("Scenes",comment: "Scene Manager comments")
let ItemsStr = NSLocalizedString("Items",comment: "Item Manager comments")
let CategoriesStr = NSLocalizedString("Categories",comment: "Category Manager comments")
let BackgroundStr = NSLocalizedString("Background",comment: "Background comments")
let TutorialStr = NSLocalizedString("Tutorial",comment: "Tutorial comments")
let RatingStr = NSLocalizedString("Rate", comment: "Rating comments")
let FeedbackStr = NSLocalizedString("Feedback",comment: "Feedback comments")
let ShareStr = NSLocalizedString("Share",comment: "Share comments")
let AboutUsStr = NSLocalizedString("About Us",comment: "AboutUs comments")


let bgWidth = UIScreen.mainScreen().bounds.width
let bgHeight = UIScreen.mainScreen().bounds.height


extension UIColor {
    class func myYellowColor() -> UIColor{
        return UIColor(red: 248/255, green: 231/255, blue: 28/255, alpha: 1)
    }
    class func myLightYellowColor() -> UIColor{
        return UIColor(red: 1, green: 1, blue: 70/255, alpha: 1)
    }
    class func myOpaqueYellowColor() -> UIColor{
        return UIColor(red: 1, green: 1, blue: 70/255, alpha: 0.3)
    }
    class func myBrownColor() -> UIColor{
        return UIColor(red: 55/255, green: 50/255, blue: 48/255, alpha: 1)
    }
    class func myOpaqueBrown() -> UIColor{
        return UIColor(red: 55/255, green: 50/255, blue: 48/255, alpha: 0.4)
    }
    class func myOpaqueGray() -> UIColor{
        return UIColor(white: 0, alpha: 0.1)
    }
    class func myOpaqueWhite() -> UIColor{
        return UIColor(white: 1, alpha: 0.1)
    }
    class func myWhiteColor() -> UIColor{
        return UIColor(white: 0.99, alpha: 1)
    }
    class func myBlueColor() -> UIColor{
        return UIColor(red: 80/255, green: 227/255, blue: 194/255, alpha: 1)//6/255, green: 250/255, blue: 225/255
    }
    class func myOpaqueBlueColor() -> UIColor{
        return UIColor(red: 80/255, green: 227/255, blue: 194/255, alpha: 0.5)
    }
    class func myLightBlueColor() -> UIColor{
        return UIColor(red: 160/255, green: 1, blue: 240/255, alpha: 1)
    }
    class func myOrangeColor() -> UIColor{
        return UIColor(red: 243/255, green: 151/255, blue: 0, alpha: 1)
    }
}

extension UILabel {
    func myParagraphSpacedText (#string: String){
        var text = NSMutableAttributedString(string: string)
        var style = NSMutableParagraphStyle()
        style.paragraphSpacing = CGFloat(8)//段间距
        text.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, text.length))//添加风格
        text.addAttribute(NSForegroundColorAttributeName, value: UIColor.myBrownColor(), range: NSMakeRange(0, text.length))//字体颜色
        self.attributedText = text
    }
    func myLineNCharSpacedText (#string: String){
        var text = NSMutableAttributedString(string: string)
        var style = NSMutableParagraphStyle()
        style.lineSpacing = CGFloat(8)//行间距
        style.alignment = NSTextAlignment.Center//居中对齐
        text.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, text.length))//添加风格
        text.addAttribute(NSForegroundColorAttributeName, value: UIColor.myBrownColor(), range: NSMakeRange(0, text.length))//字体颜色
        text.addAttribute(NSKernAttributeName, value: 2, range: NSMakeRange(0, text.length))//字间距
        self.attributedText = text
    }
}


class HomeViewController: UIViewController,UISearchBarDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UMSocialUIDelegate {
    
    //MARK: Properties
    var items:[ItemMO] = ItemMO.fetchItems() ?? []
    var selectedCategory:CategoryMO?
    var isLeftViewOpen = false
    var version = (UIDevice.currentDevice().systemVersion as NSString).floatValue
    var collectionViewCategoriesH = CGFloat(65)
    var collectionViewCategoriesY = CGFloat(0)
    var collectionViewItemsY      = CGFloat(0)
    var settingViewW: CGFloat?
    var selectedCategoryItem: Int?
    var settingsSection = [DataManagementStr,MoreStr]
    var settingsData = [DataManagementStr:[ScenesStr,ItemsStr,CategoriesStr,BackgroundStr,TutorialStr], MoreStr:[RatingStr,FeedbackStr,ShareStr,AboutUsStr]]
    var settingsImgName = [DataManagementStr:["Scenes","Items","CategorySelected","Background","Tutorial"], MoreStr:["Rating","Feedback","Share","AboutUs"]]
    var searchedContent:String = ""
    
    //MARK: CenterView Properties
    var centerView : UIView?
    lazy var naviTitleLable : UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = appName
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(20)
        return naviTitleLable
    }()
    var naviSettingBtn : UIButton?
    var bgImage = UIImageView()
    var searchBar = UISearchBar()
    var addNewBtnBg: UIView?
    var addNewBtn = UIButton()
    var addNewLab1 = UILabel()
    var collectionViewItems: UICollectionView?
    var collectionViewItemsBgView = [UIView](count: 4, repeatedValue: UIView())
    var collectionViewCategories: UICollectionView?
    var flowLayOut4Items = UICollectionViewFlowLayout()
    var flowLayOut4Categories = UICollectionViewFlowLayout()
    var categoryIndicator: UIView?
    var tapGestureRecognizer: UITapGestureRecognizer?
    var tapView: UIView?
    var timer = NSTimer()

    //MARK: SettingView Properties
    var settingView   : UIView?
    var leftBarView   : UIView?
    var leftTitleLabel: UILabel?
    var iconImgView   : UIImageView?
    var tableView     : UITableView?
    
    
    //MARK: Init and Deinit
    required init(coder aDecoder: NSCoder) {
        //Do any init setup here
        super.init(coder: aDecoder)
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    //MARK: UISearchBar and delegate
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.becomeFirstResponder()
        //键盘resign
    }
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        searchBar.resignFirstResponder()
        self.collectionViewCategories?.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
        self.collectionViewCategories?.selectItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), animated: false, scrollPosition: UICollectionViewScrollPosition.Left)
        self.selectedCategoryItem = 0
        self.collectionViewCategories?.reloadData()
        indicatorMovement(toCategoryItem: 0)
        self.items = ItemMO.fetchItems(searchedContext: self.searchBar.text) ?? []
        self.collectionViewItems?.reloadData()
    }
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        SZLOG("did end end editing")
        self.searchedContent = searchBar.text
        if self.selectedCategory == nil {
            self.items = ItemMO.fetchItems(searchedContext: searchBar.text) ?? []
        }else{
            self.items = ItemMO.fetchItems(searchedContext: searchBar.text, category: self.selectedCategory!) ?? []
        }
        self.collectionViewItems?.reloadData()
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar){
        SZLOG("did begin editing")
        self.collectionViewItems?.reloadData()
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar){
        self.collectionViewItems?.reloadData()
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.collectionViewCategories?.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
        self.collectionViewCategories?.selectItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), animated: false, scrollPosition: UICollectionViewScrollPosition.Left)
        self.selectedCategoryItem = 0
        self.collectionViewCategories?.reloadData()
        indicatorMovement(toCategoryItem: 0)
        self.items = ItemMO.fetchItems(searchedContext: self.searchBar.text) ?? []
        self.collectionViewItems?.reloadData()
    }
    
    //MARK: UICollectionView delegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.tag == 0) {
            return self.items.count
        }else {
            return appDelegate.categories.count + 1
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerClass(CollectionViewItemsCell.self, forCellWithReuseIdentifier: "ItemCell")
        collectionView.registerClass(CollectionViewCategoryCell.self, forCellWithReuseIdentifier: "CategoryCell")
        if (collectionView.tag == 0) {
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("ItemCell", forIndexPath: indexPath) as! CollectionViewItemsCell
            cell.itemImage.image = UIImage(data: items[indexPath.item].data)
            return cell
        }else{
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryCell", forIndexPath: indexPath) as! CollectionViewCategoryCell
            cell.category.textColor = UIColor.lightGrayColor()
            if indexPath.item == 0{
                cell.category.text = defaultCatetoryAll
                cell.categoryImg.image = UIImage(named: "All")
            }else{
                cell.category.text = NSLocalizedString(appDelegate.categories[indexPath.item - 1].name,comment: "name of category")
                if let availableImage = UIImage(named: appDelegate.categories[indexPath.item - 1].name){
                    cell.categoryImg.image = availableImage
                }else{
                    cell.categoryImg.image = UIImage(named: "userCategory")
                }
            }
            if indexPath.item == selectedCategoryItem {
                cell.category.textColor = UIColor.myBrownColor()
                if selectedCategoryItem == 0{
                    cell.categoryImg.image = UIImage(named: "AllSelected")
                }else{
                    if let availableImage = UIImage(named: "\(appDelegate.categories[indexPath.item - 1].name)Selected"){
                        cell.categoryImg.image = availableImage
                    }else{
                        cell.categoryImg.image = UIImage(named: "userCategorySelected")
                    }
                }
            }
            return cell
        }
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if (collectionView.tag == 0){//物品
            naviSettingBtn?.removeFromSuperview()
            var transition = CATransition()
            transition.duration = 0.2
            transition.subtype = kCATransitionFromBottom
            self.navigationController?.view.layer.addAnimation(transition, forKey: nil)
            naviSettingBtn?.removeFromSuperview()
            //self.navigationController?.pushViewController(AddingNewViewController(), animated: false)
            var addingNewViewController = AddingNewViewController()
            addingNewViewController.scene = items[indexPath.item].containerScene
            addingNewViewController.item = items[indexPath.item]
            self.navigationController?.pushViewController(addingNewViewController, animated: false)
        }else{//分类
            selectedCategoryItem = indexPath.item
            if selectedCategoryItem == 0{
                self.items = ItemMO.fetchItems()!
            }else{
                self.selectedCategory = appDelegate.categories[indexPath.item - 1]
                self.items = ItemMO.fetchItems(searchedContext: self.searchedContent, category: self.selectedCategory!) ?? []
            }
            collectionViewItems?.reloadData()
            collectionViewCategories?.reloadData()
            collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: indexPath.item, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
            indicatorMovement(toCategoryItem: indexPath.item)
        }
    }
    
    //MARK: UITableView delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return settingsSection.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var key:String = settingsSection[section]
        return settingsData[key]?.count ?? 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        tableView.registerClass(SettingsTableCell.self, forCellReuseIdentifier: "settingsTableCell")
        var cell = tableView.dequeueReusableCellWithIdentifier("settingsTableCell", forIndexPath: indexPath) as! SettingsTableCell
        var section = settingsSection[indexPath.section]
        cell.txtLabel!.text = settingsData[section]![indexPath.row]
        cell.imgView?.image = UIImage(named: settingsImgName[section]![indexPath.row])
//        if indexPath.section == 1{
//            cell.backgroundColor = UIColor(white: 1, alpha: 0.06)
//            cell.seperator1?.backgroundColor = UIColor(white: 0.1, alpha: 1)
//            cell.seperator2?.backgroundColor = UIColor(white: 0.4, alpha: 1)
//        }
        return cell
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView()
        var headerTitleLable = UILabel(frame: CGRect(x: 10, y: 3, width: settingViewW!, height: 20))
        headerTitleLable.text = settingsSection[section]
        headerTitleLable.textColor = UIColor.whiteColor()
        headerTitleLable.font = UIFont.systemFontOfSize(14)
        headerView.addSubview(headerTitleLable)
        headerView.backgroundColor = UIColor(white: 0.5, alpha: 1)
//        if section == 0{
//            headerView.backgroundColor = UIColor(white: 0.4, alpha: 1)
//        }else{
//            headerView.backgroundColor = UIColor.lightGrayColor()
//        }
        return headerView
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        btnSettingTouched()
        SZLOG("did select setting table view")
        if indexPath.section == 0{
            switch indexPath.row{
            case 0:
                //naviSettingBtn?.removeFromSuperview()
                let sceneManager = ExistedScenesPickerController()
                sceneManager.isPushFromSettings = true
                self.navigationController?.pushViewController(sceneManager, animated: true)
            case 1:
                //naviSettingBtn?.removeFromSuperview()
                self.navigationController?.pushViewController(ItemPickerControllerDeletable(), animated: true)
            case 2:
                //naviSettingBtn?.removeFromSuperview()
                self.navigationController?.pushViewController(CategoryPickerControllerDeletable(), animated: true)
            case 3:
                //naviSettingBtn?.removeFromSuperview()
                self.navigationController?.pushViewController(BackgroundPickerController(), animated: true)
            case 4:
                //naviSettingBtn?.removeFromSuperview()
                self.navigationController?.pushViewController(TutorialHomeViewController(), animated: true)
            default:
                SZLOG("unkonwn error")
            }
        }else if indexPath.section == 1{
            switch indexPath.row{
            case 1:
                //naviSettingBtn?.removeFromSuperview()
                SZLOG("feed back")
                self.navigationController?.pushViewController(FeedbackPopupViewController(), animated: true)
            case 3:
                //naviSettingBtn?.removeFromSuperview()
                self.navigationController?.pushViewController(AboutUsViewController(), animated: true)
                SZLOG("about us")
            case 0:
                
                SZLOG("rating")
                //goToRating(appID: <#Int#>)
                goToRating() //default is lazy money book
            case 2:
                SZLOG("share")
                //naviSettingBtn?.removeFromSuperview()
                UMSocialSnsService.presentSnsIconSheetView(self, appKey: UMappID, shareText:UMShareText , shareImage: nil, shareToSnsNames: [UMShareToSina,UMShareToTencent,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,UMShareToQQ,UMShareToRenren,UMShareToDouban,UMShareToEmail,UMShareToSms,UMShareToFacebook,UMShareToTwitter], delegate: self)
            default:
                SZLOG("unkonwn error")
            }
        }else{
            SZLOG("unkonwn error")
        }
        tableView.deselectRowAtIndexPath(tableView.indexPathForSelectedRow()!, animated: true)
    }
    func isDirectShareInIconActionSheet() -> Bool{
        return false
    }
    
    //MARK: UIButton & Utilities
    func btnAddNewTouched() {
        var transition = CATransition()
        transition.duration = 0.2
        transition.subtype = kCATransitionFromBottom
        self.navigationController?.view.layer.addAnimation(transition, forKey: nil)
        naviSettingBtn?.removeFromSuperview()
        self.navigationController?.pushViewController(AddingNewViewController(), animated: false)
    }
    func btnAddNewTouchDown(){
        addNewBtn.backgroundColor = UIColor(red: 248/255, green: 231/255, blue: 28/255, alpha: 0.5)
    }
    func btnAddNewTouchDragExit(){
        addNewBtn.backgroundColor = UIColor.myYellowColor()
//        addNewBtn.backgroundColor = UIColor.myLightYellowColor()
    }
    
    func btnSettingTouched(){
        searchBar.resignFirstResponder()
        var rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation.duration = 0.35
        //关闭左边窗口
        if self.isLeftViewOpen {
            rotation.toValue = -M_PI
            naviSettingBtn!.layer.addAnimation(rotation, forKey: "rotation")
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.naviSettingBtn!.frame.origin.x -= self.settingViewW!
                self.centerView!.frame.origin.x -= self.settingViewW!
            })
            tapView?.hidden = true
        }
        //打开左边窗口
        else{
            rotation.toValue = M_PI
            //rotation.autoreverses = true
            //rotation.cumulative = true
            //rotation.repeatCount = 2
            naviSettingBtn!.layer.addAnimation(rotation, forKey: "rotation")
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.naviSettingBtn!.frame.origin.x += self.settingViewW!
                self.centerView!.frame.origin.x += self.settingViewW!
            })
            tapView?.hidden = false
        }
        isLeftViewOpen = !isLeftViewOpen
    }
    
    func indicatorMovement(#toCategoryItem: Int){
        UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            self.categoryIndicator?.frame.origin.x = bgWidth/5*CGFloat(toCategoryItem)
        }, completion: nil)
    }
    
    func AddNewBtnAnimation(timer: NSTimer){
        UIView.animateWithDuration(0.7, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
            self.addNewBtnBg!.alpha = 0.5
            self.addNewBtnBg!.transform = CGAffineTransformMakeScale(1.3, 1.3)
            }, completion: { (_) -> Void in
                UIView.animateWithDuration(0.7, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                    self.addNewBtnBg!.alpha = 1
                    self.addNewBtnBg!.transform = CGAffineTransformMakeScale(1, 1)
                }, completion: nil)
        })
    }
    
    func handleTapGesture(){
        var rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation.duration = 0.35
        //关闭左边窗口
        if self.isLeftViewOpen {
            rotation.toValue = -M_PI
            naviSettingBtn!.layer.addAnimation(rotation, forKey: "rotation")
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.naviSettingBtn!.frame.origin.x -= self.settingViewW!
                self.centerView!.frame.origin.x -= self.settingViewW!
            })
            tapView?.hidden = true
        }
        isLeftViewOpen = !isLeftViewOpen
    }
    
    //MARK: UIViewControllerDelegate
    func centerViewAddSubviews(){
        collectionViewItemsY = bgHeight - collectionViewCategoriesH - bgWidth/4 - 20
        collectionViewCategoriesY = bgHeight - collectionViewCategoriesH
        var addNewBtnY = (collectionViewItemsY - 64 - 50 - bgWidth*0.45)/2 + 64 + 50
        
        centerView!.layer.shadowColor = UIColor.myOpaqueBrown().CGColor
        centerView!.layer.shadowOffset = CGSizeMake(-5, 0)
        centerView!.layer.shadowOpacity = 1
        
        bgImage = UIImageView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: bgHeight))
        bgImage.image = UIImage(named: BackgroundImageStr)
        
        searchBar = UISearchBar(frame: CGRect(x: 16, y: 64 + 20, width: bgWidth - 32, height: 30))
        searchBar.layer.cornerRadius = 15
        searchBar.delegate = self
        if (version >= 7.1){
            searchBar.subviews[0].subviews[0].removeFromSuperview()
            searchBar.backgroundColor = UIColor.whiteColor()
        }else if (version == 7.0){
            searchBar.barTintColor = UIColor.whiteColor()
            searchBar.backgroundColor = UIColor.whiteColor()
        }else{
            searchBar.subviews[0].removeFromSuperview()
            searchBar.backgroundColor = UIColor.whiteColor()
        }
        
        addNewBtn = UIButton(frame: CGRect(x: bgWidth*0.275 + 14, y: addNewBtnY + 14, width: bgWidth*0.45 - 28, height: bgWidth*0.45 - 28))
        addNewBtn.backgroundColor = UIColor.myLightYellowColor()
        addNewBtn.setBackgroundImage(UIImage(named: "Z"), forState: UIControlState.Normal)
        addNewBtn.layer.cornerRadius = addNewBtn.frame.height/2
        addNewBtn.layer.masksToBounds = true
        addNewBtn.layer.borderWidth = 5
        addNewBtn.layer.borderColor = UIColor.myOpaqueWhite().CGColor
        addNewBtn.addTarget(self, action: "btnAddNewTouchDown", forControlEvents: UIControlEvents.TouchDown)
        addNewBtn.addTarget(self, action: "btnAddNewTouchDragExit", forControlEvents: UIControlEvents.TouchDragExit)
        addNewBtn.addTarget(self, action: "btnAddNewTouched", forControlEvents: UIControlEvents.TouchUpInside)
        addNewBtnBg = UIView(frame: CGRect(x: bgWidth*0.275, y: addNewBtnY, width: bgWidth*0.45, height: bgWidth*0.45))
        addNewBtnBg?.backgroundColor = UIColor.clearColor()
        addNewBtnBg?.layer.cornerRadius = addNewBtnBg!.frame.height/2
        addNewBtnBg?.layer.borderWidth = 10
        addNewBtnBg?.layer.borderColor = UIColor.myOpaqueYellowColor().CGColor
        //addNewBtnBg?.layer.borderColor = UIColor.myOpaqueGray().CGColor
        addNewLab1 = UILabel(frame: CGRect(x: 0, y: bgWidth*0.225 - 60, width: bgWidth*0.45 - 28, height: 80))
        addNewLab1.text = "+"
        addNewLab1.textColor = UIColor.lightGrayColor()
//        addNewLab1.textColor = UIColor.myBrownColor()
        addNewLab1.textAlignment = NSTextAlignment.Center
        addNewLab1.font = UIFont.systemFontOfSize(80)
        addNewLab1.shadowOffset = CGSizeMake(1, 1)
        addNewLab1.shadowColor = UIColor.whiteColor()
        //addNewBtn.addSubview(addNewLab1)
        
        flowLayOut4Items.itemSize = CGSizeMake(bgWidth/4, bgWidth/4)
        flowLayOut4Items.minimumInteritemSpacing = 0
        flowLayOut4Items.minimumLineSpacing = 0
        flowLayOut4Items.scrollDirection = UICollectionViewScrollDirection.Horizontal
        collectionViewItems = UICollectionView(frame: CGRect(x: 0, y: collectionViewItemsY, width: bgWidth, height: bgWidth/4), collectionViewLayout: flowLayOut4Items)
        collectionViewItems?.backgroundColor = UIColor.clearColor()
        collectionViewItems!.dataSource = self
        collectionViewItems!.delegate = self
        collectionViewItems!.bounces = false
        collectionViewItems!.tag = 0
        collectionViewItems!.showsHorizontalScrollIndicator = false
        for i in 0...3 {
            collectionViewItemsBgView[i] = UIView(frame: CGRect(x: bgWidth/4*CGFloat(i), y: collectionViewItemsY, width: bgWidth/4 - 5, height: bgWidth/4))
            collectionViewItemsBgView[i].backgroundColor = UIColor.myOpaqueGray()
            centerView!.addSubview(collectionViewItemsBgView[i])
        }
        
        flowLayOut4Categories.itemSize = CGSizeMake(bgWidth/5, 50)
        flowLayOut4Categories.minimumInteritemSpacing = 0
        flowLayOut4Categories.scrollDirection = UICollectionViewScrollDirection.Horizontal
        flowLayOut4Categories.minimumLineSpacing = 0
        collectionViewCategories = UICollectionView(frame: CGRect(x: 0, y: collectionViewCategoriesY, width: bgWidth, height: collectionViewCategoriesH), collectionViewLayout: flowLayOut4Categories)
        collectionViewCategories?.backgroundColor = UIColor(white: 1, alpha: 0.7)
        collectionViewCategories!.dataSource = self
        collectionViewCategories!.delegate = self
        collectionViewCategories!.bounces = false
        collectionViewCategories!.tag = 1
        collectionViewCategories!.layer.shadowOffset = CGSizeMake(10, -10)
        collectionViewCategories!.layer.shadowOpacity = 0.8
        collectionViewCategories!.layer.shadowColor = UIColor(white: 1, alpha: 0.7).CGColor
        collectionViewCategories?.showsHorizontalScrollIndicator = false
        selectedCategoryItem = 0
        
        categoryIndicator = UIView(frame: CGRect(x: 0, y: collectionViewCategoriesH - 3, width: bgWidth/5, height: 2))//y: bgHeight - 3
        categoryIndicator!.backgroundColor = UIColor.myBrownColor()
        categoryIndicator!.layer.cornerRadius = categoryIndicator!.frame.height/2
        collectionViewCategories!.addSubview(categoryIndicator!)
        
        tapView = UIView(frame: CGRect(x: 0, y: 64, width: bgWidth, height: bgHeight - 64))
        tapView?.backgroundColor = UIColor.clearColor()
        tapView?.userInteractionEnabled = true
        tapView?.hidden = true
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGesture")
        tapGestureRecognizer?.numberOfTapsRequired = 1
        tapGestureRecognizer?.numberOfTouchesRequired = 1
        tapView?.addGestureRecognizer(tapGestureRecognizer!)
        
        centerView!.addSubview(bgImage)
        centerView!.sendSubviewToBack(bgImage)
        centerView!.addSubview(naviTitleLable)
        centerView!.addSubview(searchBar)
        centerView!.addSubview(addNewBtnBg!)
        centerView!.addSubview(addNewBtn)
        centerView!.addSubview(collectionViewItems!)
        centerView!.addSubview(collectionViewCategories!)
        centerView!.addSubview(tapView!)
    }
    
    func settingViewAddSubviews(){
        settingView!.backgroundColor = UIColor.darkGrayColor()
        leftBarView = UIView(frame: CGRect(x: 0, y: 0, width: settingViewW!, height: 164))
        leftBarView!.backgroundColor = UIColor.myBlueColor()
        
        leftTitleLabel = UILabel(frame: CGRect(x: 130, y: 120, width: settingViewW! - 130, height: 25))
        leftTitleLabel!.text = appName
        leftTitleLabel!.textColor = UIColor.whiteColor()
        leftTitleLabel!.font = UIFont.boldSystemFontOfSize(30)
        
        iconImgView = UIImageView(frame: CGRect(x: 0, y: 40, width: 130, height: 130))
        iconImgView!.image = UIImage(named: "Icon")//icon img name
        
        tableView = UITableView(frame: CGRect(x: 0, y: 164, width: settingViewW!, height: bgHeight - 164))
        tableView!.backgroundColor = UIColor.myWhiteColor()
        tableView!.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView!.rowHeight  = 50
        tableView!.sectionHeaderHeight = 25
        tableView!.dataSource = self
        tableView!.delegate   = self
        
        settingView!.addSubview(leftBarView!)
        settingView!.addSubview(leftTitleLabel!)
        settingView!.addSubview(iconImgView!)
        settingView!.addSubview(tableView!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingViewW = bgWidth/375*300
        settingView = UIView(frame: CGRect(x: 0, y: 0, width: settingViewW!, height: bgHeight))
        self.settingViewAddSubviews()
        self.view.addSubview(settingView!)
        
        centerView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: bgHeight))
        self.centerViewAddSubviews()
        self.view.addSubview(centerView!)
    }
    
    override func viewWillAppear(animated: Bool) {
        //Navigation Bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.tintColor = UIColor.myBrownColor()
        self.navigationController?.navigationBar.translucent = true
        self.navigationController?.navigationBarHidden = true
        
        naviSettingBtn = UIButton(frame: CGRect(x: 0, y: 20, width: 44, height: 44))
        naviSettingBtn!.setBackgroundImage(UIImage(named: "Settings"), forState: UIControlState.Normal)
        naviSettingBtn!.addTarget(self, action: "btnSettingTouched", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(naviSettingBtn!)
        
        addNewBtn.backgroundColor = UIColor.myYellowColor()
//        addNewBtn.backgroundColor = UIColor.myLightYellowColor()

        self.items = ItemMO.fetchItems() ?? []
        collectionViewItems?.reloadData()
        collectionViewCategories?.reloadData()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(3), target: self, selector: "AddNewBtnAnimation:", userInfo: nil, repeats: true)
        timer.fire()
        
        //category to 'All'
        self.collectionViewCategories?.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
        self.collectionViewCategories?.selectItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), animated: false, scrollPosition: UICollectionViewScrollPosition.Left)
        self.selectedCategoryItem = 0
        self.collectionViewCategories?.reloadData()
        indicatorMovement(toCategoryItem: 0)
        self.items = ItemMO.fetchItems(searchedContext: self.searchBar.text) ?? []
        self.collectionViewItems?.reloadData()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //self.searchBar.resignFirstResponder()
//        if self.searchBar.isFirstResponder() == true{
//            SZLOG("searchbar is first responder ")
//        }

    }
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        timer.invalidate()
        naviSettingBtn?.removeFromSuperview()
        searchBar.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
