//
//  ItemsViewController.swift
//  AlwaysForget
//
//  Created by User on 20/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

private let alertViewForDeletingItemMessage:String = NSLocalizedString("Are you sure to delete this item?",comment: "Alert View For Deleting Item: Message")

@objc protocol ItemPickerControllerDelegate{
    optional func itemsPickerController(itemPicker picker:ItemPickerController,didFinishPickingItem item:ItemMO)
    optional func itemsPickerControllerDidCancel(itemPicker picker:ItemPickerController)
    optional func enableTapGesture()
    optional func disenableTapGesture()
}
let selectStr = NSLocalizedString("Select",comment: "Select button on nav bar")

private let CategoryCollectionViewTag:Int = 9
private let collectionViewCategoriesH = CGFloat(64)

class ItemPickerController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,ItemPickerControllerDelegate {
    //MARK: Properties
    
    var filteredItems:[ItemMO] = ItemMO.fetchItems() ?? []
    
    lazy var naviBarView:UIView = {
        let naviBarView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: 64))
        naviBarView.backgroundColor = UIColor.myBlueColor()
        return naviBarView
        }()
    lazy private var itemsCollectionView:UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: bgWidth, height: bgHeight - 64 - collectionViewCategoriesH), collectionViewLayout: self.itemsCollectionViewLayout)
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
        }()
    lazy private var itemsCollectionViewLayout:UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: bgWidth/4 - 1, height: (1/4)*bgWidth - 1)
        layout.minimumInteritemSpacing = CGFloat(1)
        layout.minimumLineSpacing = CGFloat(2)
        layout.scrollDirection = UICollectionViewScrollDirection.Vertical
        return layout
        }()
    lazy private var categoryCollectionView:UICollectionView = {
        //configure the category collection view
        var collectionViewCategoriesY = CGFloat(0)
        collectionViewCategoriesY = bgHeight - collectionViewCategoriesH
        self.selectedCategoryItem = 0
        let collectionViewCategories = UICollectionView(frame: CGRect(x: 0, y: collectionViewCategoriesY, width: bgWidth, height: collectionViewCategoriesH), collectionViewLayout: self.categoryCollectionViewLayout)
        collectionViewCategories.backgroundColor = UIColor.whiteColor()
        collectionViewCategories.showsHorizontalScrollIndicator = false
        collectionViewCategories.dataSource = self
        collectionViewCategories.delegate = self
        collectionViewCategories.tag = CategoryCollectionViewTag
        return collectionViewCategories
        }()
    lazy private var categoryCollectionViewLayout:UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSizeMake(bgWidth/5, 50)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        layout.minimumLineSpacing = 0
        return layout
        }()
    lazy private var categoryIndicator: UIView = {
        let indicator = UIView(frame: CGRect(x: 0, y: collectionViewCategoriesH - 3, width: bgWidth/5, height: 2))
        indicator.backgroundColor = UIColor.myBrownColor()
        indicator.layer.cornerRadius = indicator.frame.height/2
        return indicator
        }()
    lazy var naviSelect : UIBarButtonItem = {
        let naviSelect = UIBarButtonItem(title: selectStr, style: UIBarButtonItemStyle.Plain, target: self, action: "btnSelectTouched")
        return naviSelect
    }()
    lazy var naviBin : UIButton = {
        let naviBin = UIButton(frame: CGRect(x: bgWidth - 110, y: 0, width: 44, height: 44))
        naviBin.setBackgroundImage(UIImage(named: "binSelected"), forState: UIControlState.Normal)
        naviBin.setBackgroundImage(UIImage(named: "bin"), forState: UIControlState.Highlighted)
        naviBin.addTarget(self, action: "btnBinTouched", forControlEvents: UIControlEvents.TouchUpInside)
        naviBin.hidden = true
        return naviBin
    }()
    lazy var naviTitleLable : UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = ItemsStr
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(20)
        return naviTitleLable
        }()
    var isNaviSelectBtnSelected = false{
        willSet{
            if newValue == true{
                naviSelect.title = "Cancel"
                naviBin.hidden = false
                if self.filteredItems.count != 0{
//                    for i in 0...self.filteredItems.count - 1{
//                        var cell = itemsCollectionView.cellForItemAtIndexPath(NSIndexPath(forItem: i, inSection: 0)) as! ItemCollectionViewCell
//                        cell.deleteBtn?.hidden = true
//                        cell.isDeleteBtnHidden = true
//                        cell.longPressGestureRecognizer?.enabled = false
//                        cell.selectBtn.hidden = false
//                        cell.isSelectBtnSelected = false
//                    }
                }
            }else{
                naviSelect.title = "Select"
                naviBin.hidden = true
                if self.filteredItems.count != 0{
//                    for i in 0...self.filteredItems.count - 1{
//                        var cell = itemsCollectionView.cellForItemAtIndexPath(NSIndexPath(forItem: i, inSection: 0)) as! ItemCollectionViewCell
//                        cell.longPressGestureRecognizer?.enabled = true
//                        cell.selectBtn.hidden = true
//                    }
                }
            }
        }
    }
    //alertView for deleting scenes
    lazy var alertViewForDeletingItem:UIAlertView? = {
        return UIAlertView(title: alertViewForDeletingItemTitle, message: "", delegate: self, cancelButtonTitle: cancelStr, otherButtonTitles: popupOkBtnStr)
    }()
    
    weak var delegate:ItemPickerControllerDelegate?
    private var didSelectItem:Bool = false
    private var selectedCategoryItem:Int?
    private var selectedCategory:CategoryMO?
    var tapGestureRecognizer: UITapGestureRecognizer?
    
    
    //MARK: UIViewController Delegate and related func
    override func viewWillAppear(animated: Bool) {
        notificationCenter.addObserver(self, selector: "updateItemCollectionView", name: "DeleteItem", object: nil)
    }
    override func viewWillDisappear(animated: Bool) {
        notificationCenter.removeObserver(self)
        naviBin.removeFromSuperview()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.myWhiteColor()

        //navigation bar
        self.view.addSubview(naviBarView)
        self.view.addSubview(naviTitleLable)
        self.navigationItem.rightBarButtonItem = naviSelect
        self.navigationController?.navigationBar.addSubview(naviBin)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGesture")
        tapGestureRecognizer?.enabled = false
        //itemsCollectionView.addGestureRecognizer(tapGestureRecognizer!)
        categoryCollectionView.addSubview(categoryIndicator)
        self.view.addSubview(self.itemsCollectionView)
        self.view.addSubview(self.categoryCollectionView)

    }
    
    
    //MARK: Utilities
    func updateItemCollectionView(){
        if selectedCategoryItem == 0{
            self.filteredItems = ItemMO.fetchItems() ?? []
        }else{
            self.filteredItems = ItemMO.fetchItems(searchedContext: emptyStr, category: self.selectedCategory!) ?? []
        }
        appDelegate.saveContext()
        self.itemsCollectionView.reloadData()
    }
    
    func btnSelectTouched(){
        isNaviSelectBtnSelected = !isNaviSelectBtnSelected
    }
    
    func btnBinTouched(){
        self.alertViewForDeletingItem?.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex == 0 {
            SZLOG("cancel button click")
            itemsCollectionView.reloadData()
        }else{
            SZLOG("confirm button click")
            SZLOG(self.filteredItems.count)
            itemsCollectionView.reloadData()
        }
    }
    
    //MARK: Gesture handler
    func handleTapGesture(){
        if self.filteredItems.count != 0{
            for i in 0...self.filteredItems.count - 1{
                var cell = itemsCollectionView.cellForItemAtIndexPath(NSIndexPath(forItem: i, inSection: 0)) as? ItemCollectionViewCell
                cell?.deleteBtn?.hidden = true
                cell?.isDeleteBtnHidden = true
            }
        }
    }
    
    //MARK: ItemPickerControllerDelegate
    func enableTapGesture() {
        tapGestureRecognizer?.enabled = true
    }
    
    func disenableTapGesture() {
        tapGestureRecognizer?.enabled = false
    }
    

    
    //MARK: Collection View Datasource and delegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if collectionView.tag == self.categoryCollectionView.tag {
        //category collection view
            return (appDelegate.categories.count ?? 0) + 1
        }else{
        // items collection view
            return (self.filteredItems.count ?? 0) 
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        // category collection view
        if collectionView.tag == self.categoryCollectionView.tag{
            collectionView.registerClass(CollectionViewCategoryCell.self, forCellWithReuseIdentifier: "CategoryCell")
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryCell", forIndexPath: indexPath) as! CollectionViewCategoryCell
            cell.category.textColor = UIColor.lightGrayColor()
            if indexPath.item == 0{
                cell.category.text = defaultCatetoryAll
                cell.categoryImg.image = UIImage(named: "All")
            }else{
                cell.category.text = NSLocalizedString(appDelegate.categories[indexPath.item - 1].name,comment: "name of category")
                if let availableImage = UIImage(named: appDelegate.categories[indexPath.item - 1].name){
                    cell.categoryImg.image = availableImage
                }else{
                    cell.categoryImg.image = UIImage(named: "userCategory")
                }
            }
            if indexPath.item == selectedCategoryItem {
                cell.category.textColor = UIColor.myBrownColor()
                if selectedCategoryItem == 0{
                    cell.categoryImg.image = UIImage(named: "AllSelected")
                }else{
                    if let availableImage = UIImage(named: "\(appDelegate.categories[indexPath.item - 1].name)Selected"){
                        cell.categoryImg.image = availableImage
                    }else{
                        cell.categoryImg.image = UIImage(named: "userCategorySelected")
                    }
                }
            }
            return cell
        }else{
            //items collection view
            self.itemsCollectionView.registerClass(ItemCollectionViewCell.self, forCellWithReuseIdentifier: "ItemCellID")
            let cell = self.itemsCollectionView.dequeueReusableCellWithReuseIdentifier("ItemCellID", forIndexPath: indexPath) as! ItemCollectionViewCell
            cell.delegate = self
            cell.assoicatedItemMO = self.filteredItems[indexPath.item]
            cell.imgView?.image = UIImage(data: cell.assoicatedItemMO.data)
            cell.isSelectBtnSelected = false
            SZLOG(self.filteredItems.count)
            
            return cell
        }
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        // category collection view
        if collectionView.tag == self.categoryCollectionView.tag{
            selectedCategoryItem = indexPath.item
            if selectedCategoryItem == 0{
                self.filteredItems = ItemMO.fetchItems() ?? []
            }else{
                self.selectedCategory = appDelegate.categories[indexPath.item - 1]
                self.filteredItems = ItemMO.fetchItems(searchedContext: emptyStr, category: self.selectedCategory!) ?? []
            }
            self.itemsCollectionView.reloadData()
            self.categoryCollectionView.reloadData()
            collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: indexPath.item, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
            indicatorMovement(toCategoryItem: indexPath.item)
            return
        }else{
            //items collection view
            didSelectItem = true
            let selectedCell:ItemCollectionViewCell = self.itemsCollectionView.cellForItemAtIndexPath(indexPath) as! ItemCollectionViewCell
            self.delegate?.itemsPickerController?(itemPicker: self, didFinishPickingItem: selectedCell.assoicatedItemMO)
            selectedCell.isSelectBtnSelected = !selectedCell.isSelectBtnSelected
        }
        
    }
    
    //MARK: category collection view slider
    func indicatorMovement(#toCategoryItem: Int){
        UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            self.categoryIndicator.frame.origin.x = bgWidth/5*CGFloat(toCategoryItem)
            }, completion: nil)
    }
    
    
    //MARK: Init and Deinit
    deinit{
        if self.didSelectItem == false {
            self.delegate?.itemsPickerControllerDidCancel?(itemPicker: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//protocol Deletable{
//    var selecting:Bool {get set}
//    var toBeDeletedCellIndexPaths:[NSIndexPath] {get set}
//    var toBeDeletedScenes:[SceneMO] {get set}
//    //func deleteCells(toBeDeletedCellIndexPaths indexPath:NSIndexPath,isSelectingState state:Bool)
//    func deleteCells()
//}
class ItemPickerControllerDeletable:ItemPickerController,Deletable{
    var selecting:Bool = false
    var toBeDeletedItems:[ItemMO] = []
    var toBeDeletedCellIndexPaths:[NSIndexPath] = []
    func deleteCells(){
        ItemMO.deleteItems(items: self.toBeDeletedItems)
        self.updateItemCollectionView()
    }
    override func btnSelectTouched() {
        super.btnSelectTouched()
        if self.selecting == true{
            self.selecting = false
            //self.itemsCollectionView.allowsMultipleSelection = false
            self.itemsCollectionView.reloadData()
            toBeDeletedItems = []
            toBeDeletedCellIndexPaths = []
        }else{
            self.selecting = true
            //self.itemsCollectionView.allowsMultipleSelection = true
            self.itemsCollectionView.reloadData()
        }
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if collectionView.tag == self.itemsCollectionView.tag{
            let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! ItemCollectionViewCell
            if self.selecting == true {
                cell.selectBtn.hidden = false
                if self.toBeDeletedCellIndexPaths.filter({ (ele) -> Bool in
                    if ele == indexPath{
                        return true
                    }else{
                        return false
                    }
                }).count > 0{
                    cell.isSelectBtnSelected = true
                    cell.isSelectBtnSelected = false

                }else{
                    cell.isSelectBtnSelected = false
                }
            }else{
                cell.selectBtn.hidden = true
            }
            cell.selectBtn.userInteractionEnabled = false
            for index in toBeDeletedCellIndexPaths{
                if index == indexPath{
                    cell.isSelectBtnSelected = true
                    break
                }else{
                    cell.isSelectBtnSelected = false
                }
            }
            return cell
        }else{
            return super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
        }
    }
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
        if self.selecting == true{
            var cell = collectionView.cellForItemAtIndexPath(indexPath) as! ItemCollectionViewCell
            cell.isSelectBtnSelected = !cell.isSelectBtnSelected
            if cell.isSelectBtnSelected{
                self.toBeDeletedCellIndexPaths.append(indexPath)
                self.toBeDeletedItems.append((collectionView.cellForItemAtIndexPath(indexPath) as! ItemCollectionViewCell).assoicatedItemMO)
            }else{
                var i = 0
                var indexInArray = 0
                if self.toBeDeletedCellIndexPaths.filter({ (ele) -> Bool in
                    if ele == indexPath{
                        indexInArray = i
                        i++
                        return true
                    }
                    i++
                    return false
                }).count > 0{
                    self.toBeDeletedCellIndexPaths.removeAtIndex(indexInArray)
                    self.toBeDeletedItems.removeAtIndex(indexInArray)
                }
            }
        }
    }
    override func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1{
            self.deleteCells()
            toBeDeletedItems = []
            toBeDeletedCellIndexPaths = []
        }
    }
}
