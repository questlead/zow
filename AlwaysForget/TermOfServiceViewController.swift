//
//  AboutUsViewController.swift
//  AlwaysForget
//
//  Created by User on 13/08/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit
private let TermOfUse:String = ""
class TermOfServiceViewController: UIViewController {

    lazy var naviBarView:UIView = {
        let naviBarView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: 64))
        naviBarView.backgroundColor = UIColor.myBlueColor()
        return naviBarView
        }()
    lazy private var aboutUsWebView: UIWebView = {
        let view = UIWebView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: bgHeight))
        //view.backgroundColor = UIColor.yellowColor()
        return view
        }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.myWhiteColor()
        self.view.addSubview(self.aboutUsWebView)
        loadDocument(documentName: "Zow_Term Of Service", inView: self.aboutUsWebView)
        self.view.addSubview(naviBarView)

        // Do any additional setup after loading the view.
    }
    func loadDocument(documentName docNm:String, inView webView:UIWebView){
        let path = NSBundle.mainBundle().pathForResource(docNm, ofType: "html", inDirectory: nil, forLocalization: "Base")
        //SZLOG(path)
        let url:NSURL = NSURL(fileURLWithPath: path!)!
        let request:NSURLRequest = NSURLRequest(URL: url)
        webView.loadRequest(request)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
