//
//  DetailPopupViewController.swift
//  AlwaysForget
//
//  Created by User on 24/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//

import UIKit

private let popupWinTitleText = NSLocalizedString("Detail",comment: "Input Detail")

class DetailPopupViewController: PopupViewController {
    //MARK: Properties
    var textView:UITextView!
    var detail: String?
    var finalOkBtn: UIButton?
    weak var delegate: FPPopoverControllerDelegate?
    var popupY = bgHeight*100.0/667.0
    var height:CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popupWinTitle.text = popupWinTitleText
        
        textView = UITextView(frame: CGRect(x: 10, y: titleH + 5, width: popupW - 20, height: popupH - titleH - btnH - 10))
        textView.backgroundColor = UIColor.whiteColor()
//        textView.layer.cornerRadius = 4
//        textView.layer.borderWidth = 1
//        textView.layer.borderColor = UIColor.myOpaqueGray().CGColor
        textView.font = UIFont.systemFontOfSize(25)
        textView.text = detail
        
        SZLOG("detail=\(detail)")
        
        finalOkBtn = UIButton(frame: CGRect(x: 0, y: popupH - btnH, width: popupW, height: btnH))
        finalOkBtn!.setTitleColor(UIColor.myBrownColor(), forState: UIControlState.Normal)
        finalOkBtn!.setTitle(popupOkBtnStr, forState: UIControlState.Normal)
        finalOkBtn!.addTarget(self, action: "finalOkBtnTouched", forControlEvents: UIControlEvents.TouchUpInside)
        
        nextBtn.hidden = true
        okBtn.hidden = true
        lineVertical.hidden = true

        self.view.addSubview(textView)
        self.view.addSubview(finalOkBtn!)
        
        
        //键盘监听
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func finalOkBtnTouched() {
        detail = textView.text
        SZLOG("detail=\(detail)")
        self.delegate?.parentPopupDismiss!(PopupWinType.DetailPopupWin)
        self.delegate?.saveCategoryAndDetail!(PopupWinType.DetailPopupWin)
        textView.resignFirstResponder()
    }
    
    
    func keyboardWillShow(aNotification: NSNotification){
        var userInfo:NSDictionary = aNotification.userInfo!
        var aValue:NSValue = userInfo.objectForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        var keyboardRect:CGRect = aValue.CGRectValue()
        height = keyboardRect.size.height
        var width = keyboardRect.size.width
        SZLOG("keyboard height=\(height!)")
        if (bgHeight - height!) < (popupY + popupH) {
            finalOkBtn!.frame.origin.y = bgHeight - height! - popupY - btnH
            lineHorrizontalDown.frame.origin.y = bgHeight - height! - popupY - btnH
            var txtViewH = bgHeight - height! - popupY - btnH - textView.frame.origin.y - 5
            var txt = textView.text
            textView.removeFromSuperview()
            textView = UITextView(frame: CGRect(x: 10, y: titleH + 5, width: popupW - 20, height: txtViewH))
            textView.backgroundColor = UIColor.whiteColor()
            textView.font = UIFont.systemFontOfSize(25)
            textView.text = txt
            textView.becomeFirstResponder()
            self.view.addSubview(textView)
            self.view.bringSubviewToFront(finalOkBtn!)
            self.view.bringSubviewToFront(lineHorrizontalDown)
        }
    }

    func keyboardWillHide(aNotification: NSNotification){
        if (bgHeight - height!) < (popupY + popupH) {
            finalOkBtn!.frame.origin.y = popupH - btnH
            lineHorrizontalDown.frame.origin.y = popupH - btnH
            var txt = textView.text
            textView.removeFromSuperview()
            textView = UITextView(frame: CGRect(x: 10, y: titleH + 5, width: popupW - 20, height: popupH - titleH - btnH - 10))
            textView.backgroundColor = UIColor.whiteColor()
            textView.font = UIFont.systemFontOfSize(25)
            textView.text = txt
            self.view.addSubview(textView)
        }
    }
    
    
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        textView.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
