//
//  CategoryPickerController.swift
//  AlwaysForget
//
//  Created by User on 22/07/2015.
//  Copyright (c) 2015 Silversr. All rights reserved.
//


import UIKit
private let alertViewForDeletingCategoryTitle = NSLocalizedString("Confirm to delele?",comment: "alertViewForDeletingCategoryTitle")
private let alertViewForDeletingCategoryMessage = NSLocalizedString("Default Category Cannot Be Deleted",comment: "alertViewForDeletingCategoryMessage")
@objc protocol CategoryPickerControllerDelegate{
    optional func categoryPickerController(categoryPicker picker:CategoryPickerController,didFinishPickingCategory category:CategoryMO)
    optional func categoryPickerControllerDidCancel(categoryPicker picker:CategoryPickerController)
    optional func enableTapGesture()
    optional func disenableTapGesture()
}

private let alertTitle = NSLocalizedString("Add New Category",comment: "Add New Category AlertView Title")


class CategoryPickerController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,CategoryPickerControllerDelegate{
    //MARK: Properties
    var selecting:Bool = false

    lazy var naviBarView:UIView = {
        let naviBarView = UIView(frame: CGRect(x: 0, y: 0, width: bgWidth, height: 64))
        naviBarView.backgroundColor = UIColor.myBlueColor()
        return naviBarView
    }()
    lazy var naviTitleLable : UILabel = {
        let naviTitleLable = UILabel(frame: CGRect(x: (bgWidth - 150)/2, y: 30, width: 150, height: 25))
        naviTitleLable.text = CategoriesStr
        naviTitleLable.textAlignment = NSTextAlignment.Center
        naviTitleLable.textColor = UIColor.myBrownColor()
        naviTitleLable.font = UIFont.boldSystemFontOfSize(20)
        return naviTitleLable
        }()
    lazy var naviSelect : UIBarButtonItem = {
        let naviSelect = UIBarButtonItem(title: selectStr, style: UIBarButtonItemStyle.Plain, target: self, action: "btnNaviSelectTouched")
        return naviSelect
        }()
    lazy var naviBin : UIButton = {
        let naviBin = UIButton(frame: CGRect(x: bgWidth - 110, y: 0, width: 44, height: 44))
        naviBin.setBackgroundImage(UIImage(named: "binSelected"), forState: UIControlState.Normal)
        naviBin.setBackgroundImage(UIImage(named: "bin"), forState: UIControlState.Highlighted)
        naviBin.addTarget(self, action: "btnBinTouched", forControlEvents: UIControlEvents.TouchUpInside)
        naviBin.hidden = true
        return naviBin
        }()
    var isNaviSelectBtnSelected = false{
        willSet{
            if newValue == true{
                naviSelect.title = "Cancel"
                naviBin.hidden = false
            }else{
                naviSelect.title = "Select"
                naviBin.hidden = true
            }
        }
    }
    //alertView for deleting scenes
    lazy var alertViewForDeletingCategory:UIAlertView? = {
        var alertview = UIAlertView(title: alertViewForDeletingCategoryTitle, message: alertViewForDeletingCategoryMessage, delegate: self, cancelButtonTitle: cancelStr, otherButtonTitles: popupOkBtnStr)
        alertview.tag = 1
        return alertview
        }()
    var layout:UICollectionViewFlowLayout?
    var categoryCollectionView:UICollectionView?
    weak var delegate:CategoryPickerControllerDelegate?
    private var didSelectCategory:Bool = false
    var tapGestureRecognizer: UITapGestureRecognizer?

    
    //MARK: UIViewController Delegate and related func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.myWhiteColor()
        
        self.collectionViewConfiguration()
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGesture")
        tapGestureRecognizer?.enabled = false
        //categoryCollectionView?.addGestureRecognizer(tapGestureRecognizer!)
        
        self.view.addSubview(naviBarView)
        self.view.addSubview(naviTitleLable)
        self.navigationItem.rightBarButtonItem = naviSelect
        self.navigationController?.navigationBar.addSubview(naviBin)
        self.view.addSubview(self.categoryCollectionView!)
    }
    override func viewWillAppear(animated: Bool) {
        notificationCenter.addObserver(self, selector: "updateCategoryCollectionView", name: "CategoryUpdated", object: nil)
    }
    override func viewWillDisappear(animated: Bool) {
        notificationCenter.removeObserver(self)
        naviBin.removeFromSuperview()
    }

    
    //MARK: CategoryPickerControllerDelegate
    func enableTapGesture() {
        tapGestureRecognizer?.enabled = true
    }
    func disenableTapGesture() {
        tapGestureRecognizer?.enabled = false
    }
    
    
    //MARK: Collection View Datasource and delegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return appDelegate.categories.count + 1
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        collectionView.registerClass(CategoryCollectionViewCell.self, forCellWithReuseIdentifier: "CategoryCellID")
        collectionView.registerClass(ManagerAddNewCell.self, forCellWithReuseIdentifier: "AddNewCell")

        if indexPath.item == appDelegate.categories.count{
            SZLOG(appDelegate.categories.count)
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddNewCell", forIndexPath: indexPath) as! ManagerAddNewCell
            return cell
        }else{
            let cell = self.categoryCollectionView!.dequeueReusableCellWithReuseIdentifier("CategoryCellID", forIndexPath: indexPath) as! CategoryCollectionViewCell
            cell.delegate = self
            cell.longPressGestureRecognizer?.enabled = true
            cell.assoicatedCategoryMO = appDelegate.categories[indexPath.row]

            if let availableImage = UIImage(named: "\(appDelegate.categories[indexPath.row].name)Selected"){
                cell.categoryImg.image = availableImage
            }else{
                cell.categoryImg.image = UIImage(named: "userCategorySelected")
            }
            
            cell.category.text = NSLocalizedString(appDelegate.categories[indexPath.row].name, comment: "category label text")
            return cell
        }
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        self.didSelectCategory = true
        self.delegate?.categoryPickerController?(categoryPicker: self, didFinishPickingCategory: appDelegate.categories[indexPath.row])
        if indexPath.item == appDelegate.categories.count{
            if self.selecting == false{
                addNewBtnTouched()
            }
        }
    }
    
    //MARK: AlertView Delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 0{//新建类别
            if buttonIndex == 1{
                var newCategoryName = alertView.textFieldAtIndex(0)!.text
                updateCategoryWithName(categoryName: newCategoryName)
            }else{
                SZLOG("Alert View Cancel Button Clicked.")
            }
        }else{//删除
            if buttonIndex == 1 {
                SZLOG("delete category confirmed.")
                categoryCollectionView?.reloadData()
            }else{
                categoryCollectionView!.reloadData()
            }
        }
    }
    
    //MARK: Utilities
    func collectionViewConfiguration() {
        layout = UICollectionViewFlowLayout()
        layout!.itemSize = CGSize(width: bgWidth/3, height: (1/3)*bgWidth)
        layout!.minimumInteritemSpacing = CGFloat(0)
        layout!.minimumLineSpacing = CGFloat(0)
        categoryCollectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: bgWidth, height: bgHeight - 64), collectionViewLayout: self.layout!)
        categoryCollectionView!.backgroundColor = UIColor.myWhiteColor()
        categoryCollectionView!.delegate = self
        categoryCollectionView!.dataSource = self
    }
    
    func addNewBtnTouched(){
        var alertView = UIAlertView(title: alertTitle, message: "", delegate: self, cancelButtonTitle: cancelStr, otherButtonTitles: popupOkBtnStr)
        alertView.alertViewStyle = UIAlertViewStyle.PlainTextInput
        alertView.tag = 0
        alertView.show()
    }
    
    func handleTapGesture(){
        if appDelegate.categories.count != 0{
            for i in 0...appDelegate.categories.count - 1{
                var cell = categoryCollectionView!.cellForItemAtIndexPath(NSIndexPath(forItem: i, inSection: 0)) as? CategoryCollectionViewCell
                if cell == nil{
                    SZLOG("\(i): \(cell)")
                }
                cell?.deleteBtn?.hidden = true
                cell?.isDeleteBtnHidden = true
            }
        }
    }
    
    func updateCategoryCollectionView(){
        appDelegate.saveContext()
        self.categoryCollectionView!.reloadData()
    }
    
    func btnNaviSelectTouched(){
        isNaviSelectBtnSelected = !isNaviSelectBtnSelected
    }
    
    func btnBinTouched(){
        self.alertViewForDeletingCategory?.show()
    }
    
    //update Category with new name meanwhile reload data
    private func updateCategoryWithName(categoryName name:String){
        CategoryMO.insertNewCategory(categoryName: name)
        appDelegate.saveContext()
        self.categoryCollectionView!.reloadData()
    }
    
    //MARK: Init and Deinit
    deinit{
        if self.didSelectCategory == false {
            self.delegate?.categoryPickerControllerDidCancel?(categoryPicker: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


class CategoryPickerControllerDeletable:CategoryPickerController,Deletable{
    var toBeDeletedCategories:[CategoryMO] = []
    var toBeDeletedCellIndexPaths:[NSIndexPath] = []
    func deleteCells(){
        //ItemMO.deleteItems(items: self.toBeDeletedCategories)
        for cat in self.toBeDeletedCategories{
            CategoryMO.deleteCategory(category: cat)
            //appDelegate.saveContext()
        }
        self.updateCategoryCollectionView()
    }
    override func btnNaviSelectTouched() {
        super.btnNaviSelectTouched()
        if self.selecting == true{
            self.selecting = false
            //self.categoryCollectionView!.allowsMultipleSelection = false
            self.categoryCollectionView!.reloadData()
            toBeDeletedCategories = []
            toBeDeletedCellIndexPaths = []
        }else{
            self.selecting = true
            //self.categoryCollectionView!.allowsMultipleSelection = true
            self.categoryCollectionView!.reloadData()
        }
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //if collectionView.tag == self.itemsCollectionView.tag{
        if indexPath.item == appDelegate.categories.count{
            return super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
        }
        
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! CategoryCollectionViewCell
        if self.selecting == true {
            cell.selectBtn.hidden = false
            cell.isSelectBtnSelected = false
        }else{
            cell.selectBtn.hidden = true
        }
        
        for index in toBeDeletedCellIndexPaths{
            if index == indexPath{
                cell.isSelectBtnSelected = true
                break
            }else{
                cell.isSelectBtnSelected = false
            }
        }
        return cell
        //}else{
           // return super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
        //}
    }
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        super.collectionView(collectionView, didSelectItemAtIndexPath: indexPath)
        if self.selecting == true && indexPath.item != appDelegate.categories.count{
            let cell = (collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCollectionViewCell)
            cell.isSelectBtnSelected = !cell.isSelectBtnSelected
            if cell.isSelectBtnSelected{
                self.toBeDeletedCellIndexPaths.append(indexPath)
                self.toBeDeletedCategories.append((collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCollectionViewCell).assoicatedCategoryMO)
            }else{
                var i = 0
                var indexInArray = 0
                if self.toBeDeletedCellIndexPaths.filter({ (ele) -> Bool in
                    if ele == indexPath{
                        indexInArray = i
                        i++
                        return true
                    }
                    i++
                    return false
                }).count > 0{
                    self.toBeDeletedCellIndexPaths.removeAtIndex(indexInArray)
                    self.toBeDeletedCategories.removeAtIndex(indexInArray)
                }
            }
            
        }
    }
    override func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        super.alertView(alertView, clickedButtonAtIndex: buttonIndex)
        if buttonIndex == 1{
            self.deleteCells()
            toBeDeletedCategories = []
            toBeDeletedCellIndexPaths = []
        }
    }
}


